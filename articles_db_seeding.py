import os, binascii
from os import walk
import sys
import pprint
import logging

import xmltodict
import json

import django
from django.db import IntegrityError
from langdetect import detect
from langdetect.lang_detect_exception import LangDetectException

sys.path.append('/webapps/ibis/ibis')
from ibis_core import *
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')
django.setup()

from bs4 import BeautifulSoup
# from crawl_engine.utils.articleDateExtractor import _extractFromHTMLTag as html_to_date
from ibis_core.articles.models import Article
from ibis_core.searches.models import Search
from ibis_core.tagging.models import Tag
from django.db import transaction
from django.utils.text import slugify

log = logging.getLogger(__name__)


class ArticlesFromFile:
    def __init__(self, file):
        self.file = file
        xml = open(self.file)
        soup = BeautifulSoup(xml, "lxml")
        self.nodes = soup.find_all('node')

    def get_tag_names(self):
        tagnames = []
        for tag in self.nodes[0].find_all():
            tagnames.append(tag.name)
        return tagnames

    @transaction.atomic
    def parse_xml(self):
        # articles = list()
        counter = 0
        for node in self.nodes:
            obj = Article()
            article_url = getattr(node, 'url', '').string
            if article_url is None:
                obj.article_url = 'http://nourl.com/%s' % str(binascii.b2a_hex(os.urandom(15)), 'utf-8')
                obj.title = getattr(node, 'title', '').string
                obj.body = getattr(node, 'original-text', '').string
                # obj.top_image_url = getattr(node, 'images').string
                domains = getattr(node, 'domains', '').string
                if domains:
                    obj.domains = json.dumps([domains.split(', ')])
                obj.authors = getattr(node, 'author', '').string
                channel = getattr(node, 'channel', '').string
                if channel:
                    obj.channel = determine_search_channel(channel)
                obj.summary = getattr(node, 'summary', '').string
                obj.authors = getattr(node, 'author', '').string
                publish_date_str = getattr(node, 'publish-date', '').string
                if publish_date_str is not None:
                    obj.post_date_created = BeautifulSoup(publish_date_str, 'html.parser').string
                discovery_date_str = getattr(node, 'discovery-date', '').string
                if discovery_date_str is not None:
                    obj.post_date_crawled = BeautifulSoup(discovery_date_str, 'html.parser').string
                places = getattr(node, 'places', '').string
                if places:
                    obj.locations = parse_coords(places)
                status_str = getattr(node, 'article-status', '').string
                if status_str:
                    obj.status = determine_status(status_str)

                # Detect article source language at this point.
                # If language is 'en' we save an <article.translated_title>
                # and <article.translated_body> same as <title> and <body>.
                title_lang = ''
                text_lang = ''
                if isinstance(obj.title, str):
                    try:
                        title_lang = detect(obj.title)
                    except (LangDetectException, TypeError):
                        pass
                if isinstance(obj.body, str):
                    try:
                        text_lang = detect(obj.body)
                    except (LangDetectException or TypeError):
                        pass

                if title_lang == 'en' and text_lang == 'en':
                    obj.translated_title = obj.title
                    obj.translated_body = obj.body
                    obj.translated = True
                    obj.processed = True

                obj.source_language = text_lang if text_lang == title_lang else None
                sq = Search.objects.get(id='f37fdc98-bf46-4c5f-a81a-ae3565e93f28')
                obj.search_query = sq

                with transaction.atomic():
                    try:
                        obj.save()
                        sid = transaction.savepoint()
                        if counter <= 100:
                            counter += 1
                        else:
                            transaction.savepoint_commit(sid)
                            print("Successfully saved %d articles to DB" % counter)
                            counter = 0
                    except IntegrityError as e:
                        print(e)

    def parse_categories(self):
        """
        This method parse the same XML files and lookup only <Category>,
        <Sectors>, <Hosts>, <Causative-agents>, <Pets-diseases>, <Free-tags>,
        splits all by one and saves as single Tag object
        :return:
        """
        tags_list = []
        created_counter = 0
        for node in self.nodes:
            obj = Tag()
            category_row = getattr(node, 'category', '').string
            sectors_row = getattr(node, 'sectors', '').string
            hosts_row = getattr(node, 'hosts', '').string
            causative_agents_row = getattr(node, 'causative-agents', '').string
            pests_diseases_row = getattr(node, 'pests-diseases', '').string
            free_tags_row = getattr(node, 'free-tags', '').string

            if category_row is not None:
                tags_list.extend([x for x in category_row.split(', ')])
            if sectors_row is not None:
                tags_list.extend([x for x in sectors_row.split(', ')])
            if hosts_row is not None:
                tags_list.extend([x for x in hosts_row.split(', ')])
            if causative_agents_row is not None:
                tags_list.extend([x for x in causative_agents_row.split(', ')])
            if pests_diseases_row is not None:
                tags_list.extend([x for x in pests_diseases_row.split(', ')])
            if free_tags_row is not None:
                tags_list.extend([x for x in free_tags_row.split(', ')])

        for tag_name in tags_list:
            obj, created = Tag.objects.get_or_create(
                name=tag_name,
                defaults={'slug': slugify(tag_name)}
            )
            if created:
                print("Tag [%s] created." % obj.name)
                created_counter += 1
            else:
                print("Tag [%s] already exists." % obj.name)
        log.info("%d Tag instances were created." % created_counter)

    def read_xml(self):
        """
        To find and print the node's url if it exists
        :return: nothing
        """
        for node in self.nodes:
            url = getattr(node, 'url').string
            if url:
                print("URL attr found %s" % url)

    def xml_to_json(self):
        """
        To read XML file and display parsed data as JSON
        :return: json
        """
        xml = open(self.file).read()
        return json.dumps(xmltodict.parse(xml), indent=2)

    def read_json(self):
        """
        For reading *.json file and printing it's data to console
        :return:
        """
        pp = pprint.PrettyPrinter(indent=2)
        file = open(self.file).read()
        d = json.loads(file)
        pp.pprint(d)


def seed_db():
    f = []
    for (dirpath, dirnames, filenames) in walk('/webapps/ibis/backups/new full article dump/'):
        f.extend(dirpath + x for x in filenames)

    for file in f:
        p = ArticlesFromFile(file)
        p.parse_xml()
        # p.parse_categories()


def parse_coords(row):
    # Split example:
    # "lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277,
    # lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277".
    # We should split it and clean duplicates
    row_splitted = list(set(row.split(', ')))  # output ex: [lat: 14.0583  lng: 108.277]
    locations = {
        'coordinates': []
    }
    for item in row_splitted:
        lat = float(item.split('  ')[0].split(' ')[1])
        lng = float(item.split('  ')[1].split(' ')[1])
        point = dict()
        point['lat'] = lat
        point['lng'] = lng
        locations['coordinates'].append(point)
    return json.dumps(locations)


def determine_search_channel(text):
    if text == 'Search engines':
        return 'search_engines'
    elif text == 'Social':
        return 'social'
    elif text == 'General':
        return 'general'
    elif text == 'Industry':
        return 'industry'
    elif text == 'Research':
        return 'research'
    else:
        return 'other'


def determine_status(status):
    if status == 'Promoted':
        return 'promoted'
    elif status == 'Alert':
        return 'alert'
    elif status == 'Keep':
        return 'keep'
    elif status == 'Raw':
        return 'raw'
    else:
        return 'trash'


if __name__ == "__main__":
    seed_db()

# reader = ArticlesFromFile('/home/yuri/Downloads/new full article dump/24526-112000.xml')
# reader.get_tag_names()
# reader.parse_categories()

# r = "lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277, lat: 14.0583  lng: 108.277"

# print(parse_coords(r))
