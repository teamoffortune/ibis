import os
from fabric.api import *

env.hosts = ['46.101.217.197']
env.user = 'root'


def push_changes():
    with lcd(os.getcwd()):
        local('git push origin master')


def update_project(light_update=False):
    with cd('/webapps/ibis/ibis'):
        run('git pull origin master')
        with prefix('source ../bin/activate'):
            run('pip install -r requirements/production.txt')
            run('python manage.py migrate')
            if not light_update:
                run('python manage.py collectstatic --noinput')


def restart_webserver():
    sudo('supervisorctl restart ibis')


def deploy():
    push_changes()
    update_project()
    restart_webserver()


def light_update():
    push_changes()
    update_project(light_update=True)
    restart_webserver()
