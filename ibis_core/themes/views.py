from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.postgres.search import SearchVector
from django.core.urlresolvers import reverse
from django.db import transaction
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView

from ibis_core.common.constants import PUBLIC
from ibis_core.common.mixins import UserThemeModeratorPermission
from ibis_core.common.utils import get_searches_list_for_themes, get_articles_list_for_theme
from ibis_core.groups.models import Group
from ibis_core.issues.models import Issue
from ibis_core.themes.models import Theme, ThemeUsers
from ibis_core.users.models import User


class ThemeCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = 'themes.add_theme'
    model = Theme
    fields = ['name', 'description']

    def form_valid(self, form):
        # form.instance.author = self.request.user
        self.object = form.save()
        theme_moderator = ThemeUsers(
            theme=self.object,
            user=self.request.user,
            is_moderator=True
        )
        theme_moderator.save()
        return super(ThemeCreateView, self).form_valid(form)

    def get_success_url(self):
        # Sends the user back to the themes list
        return reverse('themes:list')


class ThemeAddExistingGroupView(LoginRequiredMixin, UserThemeModeratorPermission, FormView):
    permission_required = 'themes.change_theme'
    permission_denied_message = 'You have no permission to add group to the theme.'
    raise_exception = True
    template_name = 'groups/groups_select_list.html'

    def __init__(self):
        super(ThemeAddExistingGroupView, self).__init__()
        self.theme_instance = None

    def get_theme_instance(self):
        if self.theme_instance is not None:
            return self.theme_instance
        theme_slug = self.kwargs.get('slug')
        return get_object_or_404(Theme, slug=theme_slug)

    def get_queryset(self):
        """
        We should compose list of groups to select except existed theme groups
        """
        self.theme_instance = self.get_theme_instance()
        exclude_list = self.theme_instance.groups.values_list(
            'id',
            flat=True
        )
        return Group.objects.exclude(id__in=exclude_list)

    def get_context_data(self, **kwargs):
        """
        Get the context for this view.
        """
        queryset = self.get_queryset()
        context = {'paginator': None, 'page_obj': None, 'is_paginated': False,
                   'object_list': queryset, 'group_list': queryset}

        context.update(kwargs)
        if 'view' not in kwargs:
            context['view'] = self
        return context

    def get_success_url(self):
        return reverse(
            'themes:detail',
            kwargs={'slug': self.get_theme_instance().slug}
        )

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        groups_id_list = self.request.POST.getlist('_selected_groups', None)
        self.theme_instance = self.get_theme_instance()
        if self.theme_instance and groups_id_list and len(groups_id_list):
            self.theme_instance.groups.add(*Group.objects.filter(id__in=groups_id_list).all())

        return HttpResponseRedirect(self.get_success_url())


class ThemeUpdateView(LoginRequiredMixin, UserThemeModeratorPermission, UpdateView):
    template_name_suffix = '_update_form'
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = 'themes.change_theme'
    model = Theme
    fields = ['name', 'description']

    def get_success_url(self):
        # Sends the user back to the themes list
        return reverse('themes:list')


class ThemeListView(LoginRequiredMixin, ListView):
    model = Theme

    def get_queryset(self):
        # We should return themes where user is member
        if self.request.user.is_superuser:
            qs = super(ThemeListView, self).get_queryset()
        else:
            qs = self.model.objects.filter(
                members__user=self.request.user).all()

        if 'q' in self.request.GET:
            q_param = self.request.GET.get('q')
            qs = qs.filter(Q(name__icontains=q_param) | Q(description__icontains=q_param))

        return qs


class ThemeDetailView(LoginRequiredMixin, DetailView):
    model = Theme

    def get_context_data(self, **kwargs):
        context = super(ThemeDetailView, self).get_context_data(**kwargs)
        theme_instance = context['object']
        context['groups'] = Group.objects.filter(
            Q(theme=theme_instance) & (Q(access=PUBLIC) | Q(members__user=self.request.user))
        ).distinct()
        context['issues'] = Issue.objects.filter(
            Q(groups__group__in=context['groups']) & (Q(access=PUBLIC) | Q(members__user=self.request.user))
        ).distinct()
        context['searches'] = get_searches_list_for_themes([theme_instance.slug])
        context['articles'] = get_articles_list_for_theme(theme_instance)

        context.update(kwargs)
        return context


class ThemeUsersAddView(LoginRequiredMixin, UserThemeModeratorPermission,
                        CreateView):
    """
    CBV for adding selected users to group members
    """
    permission_required = 'themes.add_themeusers'
    permission_denied_message = 'You have no permission to add group member.'
    model = ThemeUsers
    raise_exception = True
    template_name = 'users/user_select_list.html'
    fields = ['is_moderator']

    def __init__(self):
        super(ThemeUsersAddView, self).__init__()
        self.theme_instance = None

    def get_queryset(self):
        """
        We should compose list of user to select except existed group members
        """
        theme_slug = self.kwargs.get('slug')
        self.theme_instance = get_object_or_404(Theme, slug=theme_slug)
        exclude_list = self.theme_instance.members.values_list(
            'user_id',
            flat=True
        )
        return User.objects.exclude(id__in=exclude_list)

    def get_context_data(self, **kwargs):
        """
        Get the context for this view.
        """
        queryset = self.get_queryset()
        context = {'paginator': None, 'page_obj': None, 'is_paginated': False,
                   'object_list': queryset, 'user_list': queryset}

        context.update(kwargs)
        return super(ThemeUsersAddView, self).get_context_data(**context)

    def get_theme_instance(self):
        theme_slug = self.kwargs.get('slug')
        return get_object_or_404(Theme, slug=theme_slug)

    def get_success_url(self):
        if self.theme_instance is None:
            self.theme_instance = self.get_theme_instance()
        return reverse(
            'themes:detail',
            kwargs={'slug': self.theme_instance.slug}
        )

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        users_id_list = self.request.POST.getlist('_selected_users', None)
        self.theme_instance = self.get_theme_instance()
        if self.theme_instance and users_id_list and len(users_id_list):
            bulk_list = [
                ThemeUsers(
                    theme_id=self.theme_instance.id, user_id=user_id
                ) for user_id in users_id_list
                ]
            ThemeUsers.objects.bulk_create(bulk_list)
        return HttpResponseRedirect(self.get_success_url())


class ThemeUserToModerator(LoginRequiredMixin, UserThemeModeratorPermission,
                           UpdateView):
    permission_denied_message = "You doesn't have enough permissions."
    permission_required = 'themes.change_themeusers'
    model = ThemeUsers
    fields = ['is_moderator']

    def get_success_url(self):
        themeuser_instance = self.get_object()
        return reverse(
            'themes:detail',
            kwargs={'slug': themeuser_instance.theme.slug}
        )


class ThemeUsersDeleteView(LoginRequiredMixin, UserThemeModeratorPermission,
                           DeleteView):
    permission_required = 'themes.delete_themeusers'
    permission_denied_message = 'You have no permission to remove member.'
    model = ThemeUsers
    raise_exception = True

    def get_success_url(self):
        themeuser_instance = self.get_object()
        return reverse(
            'themes:detail',
            kwargs={'slug': themeuser_instance.theme.slug}
        )


