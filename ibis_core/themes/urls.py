# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from ibis_core.groups.views import GroupCreateView
from . import views

urlpatterns = [
    # URL pattern for the ThemeListView
    url(
        regex=r'^$',
        view=views.ThemeListView.as_view(),
        name='list'
    ),

    # URL pattern for the CreateTheme
    url(
        regex=r'^~new/$',
        view=views.ThemeCreateView.as_view(),
        name='create'
    ),

    # URL pattern for the UpdateTheme
    url(
        regex=r'^(?P<slug>[\w-]+)/~update$',
        view=views.ThemeUpdateView.as_view(),
        name='update'
    ),

    # Urls for user management inside theme
    url(
        regex=r'^(?P<slug>[\w-]+)/~add_members/$',
        view=views.ThemeUsersAddView.as_view(),
        name='add_members'
    ),

    url(
        regex=r'^(?P<slug>[\w-]+)/~to_moderator/(?P<pk>[0-9]+)$',
        view=views.ThemeUserToModerator.as_view(),
        name='to_moderator'
    ),

    url(
        regex=r'^(?P<slug>[\w-]+)/~remove_member/(?P<pk>[0-9]+)$',
        view=views.ThemeUsersDeleteView.as_view(),
        name='remove_theme_member'
    ),

    # Url for creating theme groups
    url(
        regex=r'^(?P<slug>[\w-]+)/~add_group/$',
        view=GroupCreateView.as_view(),
        name='add_theme_group'
    ),

    url(
        regex=r'^(?P<slug>[\w-]+)/~add_existing_group/$',
        view=views.ThemeAddExistingGroupView.as_view(),
        name='add_theme_existing_group'
    ),

    # URL pattern for the ThemeDetailView
    url(
        regex=r'^(?P<slug>[\w-]+)$',
        view=views.ThemeDetailView.as_view(),
        name='detail'
    ),



]
