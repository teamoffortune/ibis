from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from ibis_core.themes.models import Theme


@admin.register(Theme)
class MyThemeAdmin(admin.ModelAdmin):
    search_fields = ['name']

