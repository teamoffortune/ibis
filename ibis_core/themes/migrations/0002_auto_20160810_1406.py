# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-10 14:06
from __future__ import unicode_literals

import autoslug.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('themes', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='theme',
            name='slug',
            field=autoslug.fields.AutoSlugField(always_update=True, editable=False, populate_from='name'),
        ),
    ]
