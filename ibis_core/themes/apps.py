from django.apps import AppConfig


class ThemeConfig(AppConfig):
    name = 'ibis_core.themes'
    verbose_name = "Themes"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        pass
