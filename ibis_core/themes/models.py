from django.conf import settings
from django.db import models

from ibis_core.common.models import AbstractNamedModel


class Theme(AbstractNamedModel):

    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    @property
    def moderators(self):
        return self.members.filter(is_moderator=True)

    @property
    def followers(self):
        return self.members.filter(is_moderator=False)

    def get_activity_followers(self):
        return [user_id for user_id in self.members.all().values_list('user_id', flat=True)]

    @models.permalink
    def get_absolute_url(self):
        return 'themes:detail', (), {'slug': self.slug}

    def add_follower(self, user):
        try:
            theme_user = ThemeUsers.objects.get(user=user, theme=self)
            theme_user.is_follower = True
            theme_user.save()
        except ThemeUsers.DoesNotExist:
            ThemeUsers.objects.create(user=user, theme=self, is_follower=True)


class ThemeUsers(models.Model):
    theme = models.ForeignKey('Theme', related_name='members')
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='themes'
    )
    is_moderator = models.BooleanField(default=False)
    is_follower = models.BooleanField(default=False)


