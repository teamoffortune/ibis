import json
import pprint

import celery
from django.contrib.sites.models import Site
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.core import serializers

from django.core import serializers
from ibis_core.digest.models import DailyDigest
from ibis_core.digest.tasks import send_notification_email, send_digest


@receiver(post_save, sender=DailyDigest)
def create_daily_digest(sender, instance, created, raw, **kwargs):
    if created:
        for moderator in instance.moderators:
            current_site = Site.objects.get_current().domain
            data = {
                'url': instance.get_target_url,
                'username': moderator.user.username,
                'digest': instance
            }
            text_body = render_to_string('digest/email_templates/moderator_notification.txt', data)
            html_body = render_to_string('digest/email_templates/moderator_notification.html', data)

            result = send_notification_email.apply_async(
                args=["Confirm a daily digest from %s" % instance.created, moderator.user.email],
                kwargs={'plain_body': text_body, 'html_body': html_body}
            )
        instance.moderators_notified = True
        if not raw:
            instance.save()

    # if instance.approved and not instance.sent and not raw:
    #     # Some dummy data for digest
    #     # items = [a for a in instance.articles.all()]
    #     items = instance.articles.all()
    #     data = {
    #         'articles': []
    #     }
    #     for item in items:
    #         row = dict()
    #         row['url'] = item.get_ulr_for_email_template
    #         row['translated_title'] = item.translated_title
    #         data['articles'].append(row)
    #     # arts = serializers.serialize('json', data)
    #     arts = json.dumps(data)
    #     pprint.pprint(arts, indent=4)
    #     for user in instance.group.users.all():
    #         if user.is_subscribed:
    #             result = send_digest.apply_async((arts, user.email))
    #         instance.sent = True
    #         instance.save()
