# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    # URL pattern for the DigestListView
    url(
        regex=r'^$',
        view=views.DigestListView.as_view(),
        name='list'
    ),

    # URL pattern for the DigestDetailView
    url(
        regex=r'^(?P<pk>[\w-]+)$',
        view=views.DigestDetailView.as_view(),
        name='detail'
    ),
    # URL pattern for the DigestUpdateView
    url(
        regex=r'^(?P<pk>[\w-]+)/~update$',
        view=views.DigestUpdateView.as_view(),
        name='update'
    ),

    url(
        regex=r'^(?P<pk>[\w-]+)/~approvestate$',
        view=views.DigestApproveStateView.as_view(),
        name='approvestate'
    ),

    # url(
    #     regex=r'^(?P<digest_pk>[0-9a-z-]+)/~remove_article/(?P<article_pk>[0-9a-z-]+)$',
    #     view=views.DigestRemoveArticleView.as_view(),
    #     name='remove-article-from-digest'
    # )
]
