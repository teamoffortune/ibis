from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from ibis_core.digest.models import DailyDigest


@admin.register(DailyDigest)
class DailyDigestAdmin(admin.ModelAdmin):
    list_display = ['created', 'group', 'approved']
    ordering = ['-created']
    exclude = ('articles',)
