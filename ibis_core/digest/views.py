from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import DetailView
from django.views.generic.edit import FormMixin
from django.views.generic import ListView
from django.views.generic import UpdateView

from ibis_core.articles.models import Article
from ibis_core.common.mixins import UserDigestModeratorPermission
from ibis_core.digest.forms import DigestArticleRemoveForm, DigestApproveStateForm
from ibis_core.digest.models import DailyDigest


class DigestDetailView(LoginRequiredMixin, UserDigestModeratorPermission, DetailView, FormMixin):
    model = DailyDigest
    form_class = DigestArticleRemoveForm
    permission_required = 'digest.change_dailydigest'
    permission_denied_message = 'You have no permission to change a daily digest.'
    raise_exception = True
    fields = ['is_moderator']

    def get_success_url(self):
        return reverse('digest:detail', kwargs={'pk': self.get_object().pk})

    def get_context_data(self, **kwargs):
        context = super(DigestDetailView, self).get_context_data(**kwargs)
        context['del_article_form'] = DigestArticleRemoveForm()
        context['state_form'] = DigestApproveStateForm()
        return context

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        self.object = self.get_object()
        self.article_pk = request.POST.get('article', None)
        self.state = request.POST.get('state', None)
        if self.article_pk is not None:
            form = DigestArticleRemoveForm()
            return self.form_del_article_valid(form)
        if self.state is not None:
            form = DigestApproveStateForm()
            return self.form_change_state_valid(form)

    def form_del_article_valid(self, form):
        article = get_object_or_404(Article, pk=self.article_pk)
        if article:
            self.object.articles.remove(article)
            self.object.save()
        return super(DigestDetailView, self).form_valid(form)

    def form_change_state_valid(self, form):
        self.object.approved = self.state
        self.object.save()
        return super(DigestDetailView, self).form_valid(form)


class DigestRemoveArticleView(LoginRequiredMixin, UserDigestModeratorPermission, UpdateView):
    model = DailyDigest
    permission_required = 'digest.change_dailydigest'
    permission_denied_message = 'You have no permission to change a daily digest.'
    raise_exception = True
    fields = ['group', 'articles']

    def post(self, request, *args, **kwargs):
        article_pk = self.request.POST.get('article_pk')
        digest_pk = self.request.POST.get('pk')
        digest = get_object_or_404(DailyDigest, pk=digest_pk)
        digest.articles.remove(Article.objects.filter(pk=article_pk))
        digest.save()

    def form_valid(self, form):
        self.object = form.save()



    # def get_success_url(self):
    #     return reverse('digest:detail')


class DigestListView(ListView):
    pass


class DigestUpdateView(LoginRequiredMixin, UserDigestModeratorPermission, UpdateView):
    model = DailyDigest
    permission_required = 'digest.change_dailydigest'
    permission_denied_message = 'You have no permission to change a daily digest.'
    raise_exception = True
    fields = ['group', 'articles']

    def post(self, request, *args, **kwargs):
        article_pk = self.request.POST.get('article_pk')
        digest_pk = self.request.POST.get('pk')
        digest = get_object_or_404(DailyDigest, pk=digest_pk)
        digest.articles.remove(Article.objects.filter(pk=article_pk))
        digest.save()


class DigestApproveStateView(UpdateView):
    pass
