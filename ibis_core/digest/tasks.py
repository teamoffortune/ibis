import json
import logging
import operator
import itertools
import pprint

from datetime import datetime, timedelta, date
from celery import shared_task
from celery.schedules import crontab
from celery.task import periodic_task
from django.contrib.sites.models import Site
from django.core import serializers
from django.core.exceptions import MultipleObjectsReturned
from django.core.mail import send_mail
from django.conf import settings
from django.db import IntegrityError
from django.utils.timezone import utc

from ibis_core.common.mailgun_client import MailgunClient
from django.template.loader import render_to_string
from django.db.models import Q

from ibis_core.groups.models import Group
from ibis_core.digest.models import DailyDigest
from ibis_core.articles.models import Article
from ibis_core.issues.models import Issue

logger = logging.getLogger(__name__)


# @periodic_task(
#     run_every=(crontab(minute='*/3')),
#     name='digest.tasks.prepare_digest_for_groups',
# )
# def prepare_digest_for_groups():
#     """
#     Task for forming a daily articles digest for group
#     """
#     for group in Group.objects.all():
#         try:
#             digest, created = DailyDigest.objects.get_or_create(
#                 created__day=date.today().day,
#                 group=group,
#                 defaults={'group': group}
#             )
#         except MultipleObjectsReturned:
#             return "Multiple Digest objects are on database. Please fix module."
#         # if digest.created.day != date.today().day:
#         now = datetime.utcnow().replace(tzinfo=utc)
#         delta = timedelta(days=10)
#         articles = Article.objects.filter(status='promoted').order_by('-created')[:10]
#         queryset = []
#         for article in articles:
#             queryset.append(article)
#         try:
#             # digest.articles.add(*[a for a in Article.objects.filter(
#             #     # Q(article__search_query__issues__in=group.issues.all()) &
#             #     # Q(post_date_crawled__range=[now - delta, now]) &
#             #     Q(status='promoted') | Q(status='alert')).order_by('-created')[:10]])  # Should be the articles from the group promoted/alert
#             digest.articles.add(*queryset)
#         except IntegrityError:
#             pass
#         for member in group.members.all():
#             if member.user.is_subscribed:
#                 digest.recipients.add(member.user)
#         logger.info("Digest created ID: %s." % digest.id)
#     return "Digests were created"


# @periodic_task(
#     run_every=(crontab(minute=0, hour=3)),
#     name='digest.tasks.check_all_digests'
# )
# def check_all_digests():
#     """
#     This periodic task checks all not approved digests. If the digest wasn't approved
#     during 5 hours it sends automatically without approval from moderators.
#     """
#     today = date.today()
#     digests_not_approved = DailyDigest.objects.filter(created__day=today.day, sent=False, approved=False)
#     recipients = []
#     for digest in digests_not_approved:
#         delta = datetime.utcnow().replace(tzinfo=utc) - digest.created.replace(tzinfo=utc)
#         if delta > timedelta(hours=5):
#             for user in digest.recipients.all():
#                 send_digest.apply_async(kwargs={'articles_list': digest.articles, 'email': user.email})
#                 recipients.append(user.email)
#             digest.sent = True
#             digest.save()
#     return "Successfully sent to: %s" % recipients


@periodic_task(
    run_every=(crontab(
        hour=0,
        minute=0)),
    name='digest.tasks.collect_digest'
)
def collect_digest():
    """
    This periodic task checks all today digests. Collects all articles from digests for group followers
    """
    today = date.today()
    digests = DailyDigest.objects.filter(created__day=today.day-1)
    issue_dict = dict()
    for digest in digests:
        for issue_group in digest.group.issues.all().distinct():
            issue_dict[str(issue_group.issue.id)] = {
                'articles_list': [str(a.id) for a in digest.articles.filter(
                    search_query__issues=issue_group.issue.id)],
                'users_list': [u.user.email for u in issue_group.issue.members.filter(is_follower=True)]
            }
            keys_to_remove = []
            for key, value in issue_dict.items():
                if len(issue_dict[key]['articles_list']) == 0:
                    keys_to_remove.append(key)
            for k in keys_to_remove:
                del(issue_dict[k])
    data = invert_digest_by_users(issue_dict)
    for user_email, value in data.items():
        pprint.pprint(user_email)
        pprint.pprint(value)
        send_digest.apply_async((value, user_email),)
        logger.info("Sending digest tasks to [%s] was scheduled." % user_email)
    return []


def invert_digest_by_users(digest_dict):
    res = dict()
    for issue_id, value in digest_dict.items():
        for user_id in value['users_list']:
            if user_id in res:
                res[user_id][issue_id] = value['articles_list']
            else:
                res[user_id] = {issue_id: value['articles_list']}
    return res


def serialize_articles(objects_list):
    data = {
       'articles': []
    }
    for item in objects_list:
        row = dict()
        row['url'] = item.article_url
        row['translated_title'] = item.translated_title
        row['translated_body'] = item.translated_body
        data['articles'].append(row)
    # arts = serializers.serialize('json', data)
    arts = json.dumps(data)
    # pprint.pprint(arts, indent=4)
    return data


@shared_task(
    name='digest.tasks.send_digest',
)
def send_digest(digest_data, email):
    # Email templates
    mail_title_template = "Ibis Daily Digest from %s"
    email_data = []
    # Get current site domain name. Will be passed to email template
    current_site = Site.objects.get_current().domain
    for issue_id, value in digest_data.items():
        issue = Issue.objects.get(id=issue_id)
        articles = Article.objects.filter(id__in=[a for a in value])
        row = dict()
        row['issue'] = issue.name
        row['articles'] = articles
        email_data.append(row)
    article_digest_template_text = render_to_string(
        'digest/email_templates/articles_digest.txt', {'digest': email_data, 'current_site': current_site})
    article_digest_template_html = render_to_string(
        'digest/email_templates/article_digest_new.html', {'digest': email_data, 'current_site': current_site})

    mail_title = mail_title_template % datetime.today()
    result = send_mail(mail_title, article_digest_template_text, settings.DEFAULT_FROM_EMAIL, [email],
                       html_message=article_digest_template_html)
    return "Successfully sent to: %s" % email


@shared_task
def send_notification_email(title, recipients, plain_body=None, html_body=None):
    try:
        result = send_mail(title, plain_body, settings.DEFAULT_FROM_EMAIL,
                           [recipients], html_message=html_body)
        logger.info('Successfully notified [%s]' % recipients)
    except Exception:
        result = "Some troubles with connection. Can't notify moderators."

    # return "Notification sent to: %s" % recipients
    return result
