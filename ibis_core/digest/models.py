import logging

from django.contrib.sites.models import Site
from django.core import serializers
from django.template.loader import render_to_string

from ibis_core.common.models import AbstractBaseModel
from django.db import models
import celery


class DailyDigest(AbstractBaseModel):
    moderators_notified = models.BooleanField(default=False)
    approved = models.BooleanField(default=False)
    group = models.ForeignKey('groups.Group', blank=True)
    articles = models.ManyToManyField('articles.Article', blank=True)
    recipients = models.ManyToManyField('users.User', blank=True)
    sent = models.BooleanField(default=False)

    @property
    def moderators(self):
        # digest_moderators = []
        # for group_user in self.group.moderators.all():
        #     digest_moderators.append(group_user.user)
        # return digest_moderators
        return self.group.moderators.all()

    @property
    def simple_members(self):
        return self.group.members

    @models.permalink
    def get_absolute_url(self):
        return 'digest:detail', (), {'pk': self.id}

    @property
    def get_target_url(self):
        current_site = Site.objects.get_current()
        return 'http://%s%s' % (current_site, self.get_absolute_url())

    def get_followers(self):
        return self.group.get_followers()
