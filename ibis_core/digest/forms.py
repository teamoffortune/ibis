from django import forms

from ibis_core.digest.models import DailyDigest


class DigestArticleRemoveForm(forms.Form):
    article = forms.CharField(widget=forms.HiddenInput())


class DigestApproveStateForm(forms.Form):
    state = forms.CharField(widget=forms.HiddenInput())
