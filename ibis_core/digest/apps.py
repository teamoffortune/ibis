from django.apps import AppConfig


class DigestConfig(AppConfig):
    name = 'ibis_core.digest'

    def ready(self):
        import ibis_core.digest.signals
