import json
import os
import pprint
import struct
import sys
from datetime import datetime, timedelta, date
import time
from time import strptime

import django
import logging

sys.path.append('/home/yuri/dev/dev_ibis')
from config.settings import *
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')
django.setup()

from ibis_core.digest.models import DailyDigest
from ibis_core.digest.tasks import send_digest

logger = logging.getLogger(__name__)


def check_digests():
    today = date.today()
    digests = DailyDigest.objects.filter(created__day=today.day)
    issue_dict = dict()
    for digest in digests:
        for issue_group in digest.group.issues.all().distinct():
            issue_dict[str(issue_group.issue.id)] = {
                'articles_list': [str(a.id) for a in digest.articles.filter(
                    search_query__issues=issue_group.issue.id)],
                'users_list': [u.user.email for u in issue_group.issue.members.filter(
                    is_follower=True,
                    user__is_subscribed=True)]
                # 'users_list': [u.user.email for u in issue_group.issue.members.filter(user__is_subscribed=True)]
            }
            keys_to_remove = []
            for key, value in issue_dict.items():
                if len(issue_dict[key]['articles_list']) == 0:
                    keys_to_remove.append(key)
            for k in keys_to_remove:
                del(issue_dict[k])
    data = invert_digest_by_users(issue_dict)
    for user_email, value in data.items():
        pprint.pprint(user_email)
        pprint.pprint(value)
        send_digest(value, user_email)
        logger.info("Sending digest tasks to [%s] was scheduled." % user_email)
    return []


def invert_digest_by_users(digest_dict):
    res = dict()
    for issue_id, value in digest_dict.items():
        for user_id in value['users_list']:
            if user_id in res:
                res[user_id][issue_id] = value['articles_list']
            else:
                res[user_id] = {issue_id: value['articles_list']}
    return res


if __name__ == '__main__':
    check_digests()
