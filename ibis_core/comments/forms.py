from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from django_comments_xtd.forms import XtdCommentForm
from django_comments_xtd.models import TmpXtdComment


class IssueCommentForm(XtdCommentForm):
    title = forms.CharField(
        max_length=256,
        widget=forms.TextInput(
            attrs={
                'placeholder': _('title'),
            }
        )
    )

    def __init__(self, *args, **kwargs):
        super(IssueCommentForm, self).__init__(*args, **kwargs)
        for field_name, field_obj in self.fields.items():
            field_obj.widget.attrs.update({'class': 'form-control'})

    def get_comment_create_data(self):
        data = super(IssueCommentForm, self).get_comment_create_data()
        data.update({'title': self.cleaned_data['title']})
        return data
