from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from django_comments_xtd.admin import XtdCommentsAdmin
from ibis_core.comments.models import IssueComment


class IssueCommentAdmin(XtdCommentsAdmin):
    list_display = ('thread_id', 'order', 'thread_level', 'title', 'cid', 'name', 'comment')
    list_display_links = ('cid', 'title')
    fieldsets = (
        (None,          {'fields': ('content_type', 'object_pk', 'site')}),
        (_('Content'),  {'fields': ('title', 'user', 'user_name', 'user_email',
                                    'user_url', 'comment', 'followup')}),
        (_('Metadata'), {'fields': ('submit_date', 'ip_address',
                                    'is_public', 'is_removed')}),
    )

admin.site.register(IssueComment, IssueCommentAdmin)
