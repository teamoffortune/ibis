from django.apps import AppConfig


class IssueCommentsConfig(AppConfig):
    name = 'ibis_core.comments'
