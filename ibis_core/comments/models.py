from django.db import models
from django_comments.signals import comment_will_be_posted

from django_comments_xtd.models import XtdComment


class IssueComment(XtdComment):
    title = models.CharField(max_length=256)

    @classmethod
    def tree_from_queryset(cls, queryset, with_participants=False):
        """Converts a XtdComment queryset into a list of nested dictionaries.
        The queryset has to be ordered by thread_id, order.
        Each dictionary contains two attributes::
            {
                'comment': the comment object itself,
                'children': [list of child comment dictionaries]
            }
        """

        def get_participants(comment):
            return {'likedit': comment.users_who_liked_it(),
                    'dislikedit': comment.users_who_disliked_it()}

        def add_children(children, obj):
            for item in children:
                if item['comment'].pk == obj.parent_id:
                    child_dict = {'comment': obj, 'children': []}
                    if with_participants:
                        child_dict.update(get_participants(obj))
                    item['children'].append(child_dict)
                    return True
                elif item['children']:
                    if add_children(item['children'], obj):
                        return True
            return False

        dic_list = []
        cur_dict = None
        for obj in queryset.order_by('thread_id', 'order'):
            # A new comment at the same level as thread_dict.
            if cur_dict and obj.level == cur_dict['comment'].level:
                dic_list.append(cur_dict)
                cur_dict = None
            if not cur_dict:
                cur_dict = {'comment': obj, 'children': []}
                if with_participants:
                    cur_dict.update(get_participants(obj))
                continue
            if obj.parent_id == cur_dict['comment'].pk:
                child_dict = {'comment': obj, 'children': []}
                if with_participants:
                    child_dict.update(get_participants(obj))
                cur_dict['children'].append(child_dict)
            else:
                add_children(cur_dict['children'], obj)
        if cur_dict:
            dic_list.append(cur_dict)
        return dic_list


def comment_will_be_posted_callback(**kwargs):
    request = kwargs['request']
    kwargs['comment'].ip_address = request.META.get('HTTP_REMOTE_ADDR', '127.0.0.1')

comment_will_be_posted.connect(comment_will_be_posted_callback)
