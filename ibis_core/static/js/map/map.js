/**
 * Created by yuri on 07.11.16.
 */
var map;
// Data for the markers consisting of a name, a LatLng and a zIndex for the
// order in which these markers should display on top of each other.
// var map_items = [
//   ['Bondi Beach', -33.890542, 151.274856, 4],
//   ['Coogee Beach', -33.923036, 151.259052, 5],
//   ['Cronulla Beach', -34.028249, 151.157507, 3],
//   ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
//   ['Maroubra Beach', -33.950198, 151.259302, 1]
// ];

//Create LatLngBounds object.
var latlngbounds = new google.maps.LatLngBounds();

function setMarkers(map) {
  // Adds markers to the map.

  // Marker sizes are expressed as a Size of X,Y where the origin of the image
  // (0,0) is located in the top left of the image.

  // Origins, anchor positions and coordinates of the marker increase in the X
  // direction to the right and in the Y direction down.
  // for (var i = 0; i < map_items.length; i++) {
  //     for (var j = 0; j < map_item[i].coord.length; j++) {
  //         var myLatlng = new google.maps.LatLng(map_item.lat, map_item.lng);
  //         var marker = new google.maps.Marker({
  //             position: myLatlng,
  //             map: map,
  //             // icon: image,
  //             // shape: shape,
  //             title: map_item.name,
  //             zIndex: map_item.zindex
  //         });
  //         //Extend each marker's position in LatLngBounds object.
  //         latlngbounds.extend(marker.position);
  //     }
  // }
    var collected_markers = [];
    map_items.forEach(function (entry) {
        collected_markers.push(entry.coord);
        entry.coord.forEach(function (coord) {
            console.log("LAT: " + coord.lat + " --- LNG: " + coord.lng);

            var myLatlng = new google.maps.LatLng(coord.lat, coord.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                // icon: image,
                // shape: shape,
                // title: map_item.name,
                zIndex: coord.zindex
            });
          //Extend each marker's position in LatLngBounds object.
          latlngbounds.extend(marker.position);
        });
    });

}

function initMap() {

    //Get the boundaries of the Map.
    var bounds = new google.maps.LatLngBounds();

    map = new google.maps.Map(document.getElementById('map_canvas'), {
        // center: {lat: -34.397, lng: 150.644},
        // zoom: 8
    });

    setMarkers(map);
    //Center map and adjust Zoom based on the position of all markers.
    map.setCenter(latlngbounds.getCenter());
    map.fitBounds(latlngbounds);
}


function getMarkers(item) {
    var markersArray = [];
    markersArray.push(item.coord);
}


initMap();
