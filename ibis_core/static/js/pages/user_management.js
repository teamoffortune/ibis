+function ($) {
    $('#id-um-table').bootstrapTable({
        pagination: true,
        filterControl: true
    });

    applyFilters = function(url, params){
        url = url || window.location.href || '';
        url =  url.match(/\?/) ? url : url + '?';

        for ( var key in params ) {
            var re = RegExp( ';?' + key + '=?[^&;]*', 'g' );
            url = url.replace( re, '');
            for (val_key in params[key]) {
                url += '&' + key + '=' + params[key][val_key];
            }

        }
        // cleanup url
        url = url.replace(/[;&]$/, '');
        url = url.replace(/\?[;&]/, '?');
        url = url.replace(/[;&]{2}/g, ';');
        window.location.replace( url );
    }

    $('#id-apply-filters-btn').on('click', function(){
        var filters = {};
        $('.um-filter').each(function(){
            var $this = $(this);
            if ($this.val()) {
                filters[$this[0].name] = $this.val();
            }
        });

        return applyFilters(location.href, filters);
    });


}(jQuery);
