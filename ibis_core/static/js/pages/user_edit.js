+function ($) {
    // $("select[multiple='multiple']").multiSelect();

    var ARWidget = {
        init: function(){
            this.state = {};
            this.themeSelectors = {};
            this.groupSelectors = {};
            this.issueSelectors= {};

            this.bindSelectors();

            this.url = '/api/v1/umdata';
            this.managed_user_id = managed_user_id;
            if (this.managed_user_id) {
                this.url += '?managed_user=' + managed_user_id;
            }

            $.ajax(
                url=this.url
            ).done(function(data){
                var selectedData = data['selected'];

                this.state['themes'] = data['data'];

                if (! _.isEmpty(selectedData)) {
                    this.state['themes'].forEach(function(themeInstance){
                        themeInstance.membership = [];
                        themeInstance.membership[0] = selectedData['moderator_themes'].indexOf(themeInstance.id) > -1;
                        themeInstance.membership[1] = selectedData['member_themes'].indexOf(themeInstance.id) > -1;
                        themeInstance.membership[2] = selectedData['follower_themes'].indexOf(themeInstance.id) > -1;

                        themeInstance.groups.forEach(function(groupInstance){
                            groupInstance.membership = [];
                            groupInstance.membership[0] = selectedData['moderator_groups'].indexOf(groupInstance.id) > -1;
                            groupInstance.membership[1] = selectedData['member_groups'].indexOf(groupInstance.id) > -1;
                            groupInstance.membership[2] = selectedData['follower_groups'].indexOf(groupInstance.id) > -1;

                            groupInstance.issues.forEach(function(issueInstance){
                                issueInstance.membership = [];
                                issueInstance.membership[0] = selectedData['moderator_issues'].indexOf(issueInstance.id) > -1;
                                issueInstance.membership[1] = selectedData['member_issues'].indexOf(issueInstance.id) > -1;
                                issueInstance.membership[2] = selectedData['follower_issues'].indexOf(issueInstance.id) > -1;
                            });
                        });
                    });
                } else {
                    this.state['themes'].forEach(function(themeInstance){
                        themeInstance.membership = [false, false, false];
                        themeInstance.groups.forEach(function(groupInstance){
                            groupInstance.membership = [false, false, false];
                            groupInstance.issues.forEach(function(issueInstance){
                                issueInstance.membership = [false, false, false];
                            });
                        });
                    });
                }

                this.renderSelectors();
            }.bind(this));

        },

        bindSelectors: function(){
            [
                'theme_moderator', 'group_moderator', 'issue_moderator',
                'theme_member', 'group_member', 'issue_member',
                'theme_follower', 'group_follower', 'issue_follower'
            ].forEach(function(el){
                var $selector = $('select[name=' + el + ']');
                $selector.on('change', this.selectorOnChange.bind(this));
                $selector.empty();
                if (el.indexOf('theme') > -1) {
                    this.themeSelectors[el] = $selector;
                }else if (el.indexOf('group') > -1) {
                    this.groupSelectors[el] = $selector;
                }else{
                    this.issueSelectors[el] = $selector;
                }
            }.bind(this));
        },

        processData: function(){
            var selectorsData = {};
            this.state['themes'].forEach(function(themeInstance){

            });
        },

        renderThemesSelectors: function(){
            for (var key in this.themeSelectors) {
                var $selector = this.themeSelectors[key];
                $selector.empty();

                this.state['themes'].forEach(function(themeInstance){
                    var $option = $('<option value="' + themeInstance.id + '">' + themeInstance.name + '</option>');
                    if ($selector.selector.indexOf('moderator') > -1 && themeInstance.membership[0]) {
                        $option.attr('selected', 1);
                    }
                    if ($selector.selector.indexOf('member') > -1 && themeInstance.membership[1]) {
                        $option.attr('selected', 1);
                    }
                    if ($selector.selector.indexOf('follower') > -1 && themeInstance.membership[2]) {
                        $option.attr('selected', 1);
                    }

                    if (
                        $selector.selector.indexOf('moderator') > -1 || $selector.selector.indexOf('member') > -1 ||
                        $selector.selector.indexOf('follower') > -1 && themeInstance.membership[1]
                    ) {
                        $selector.append($option);
                    }
                });
            };
        },

        getAvailableGroups: function(membershipIndex) {
            var availableGroups = _.reduce(this.state.themes, function(memo, themeInstance){
                if (themeInstance.membership[membershipIndex]) {
                    return memo.concat(themeInstance.groups);
                }else{
                    return memo
                }
            }, []);

            return _.uniq(availableGroups, false, function(groupInstance){
                return groupInstance.id;
            });
        },

        renderGroupsSelectors: function(){
            var availableGroups;

            for (var key in this.groupSelectors) {
                var $selector = this.groupSelectors[key];
                $selector.empty();

                if ($selector.selector.indexOf('moderator') > -1) {
                    availableGroups = this.getAvailableGroups(0); // 0 - moderator
                }else if ($selector.selector.indexOf('member') > -1) {
                    availableGroups = this.getAvailableGroups(1);    // 1 - member
                }else {
                    availableGroups = this.getAvailableGroups(2);  // 2 - follower
                }

                availableGroups.forEach(function(groupInstance){
                    var $option = $('<option value="' + groupInstance.id + '">' + groupInstance.name + '</option>');

                    if ($selector.selector.indexOf('moderator') > -1 && groupInstance.membership[0]) {
                        $option.attr('selected', 1);
                    }
                    if ($selector.selector.indexOf('member') > -1 && groupInstance.membership[1]) {
                        $option.attr('selected', 1);
                    }
                    if ($selector.selector.indexOf('follower') > -1 && groupInstance.membership[2]) {
                        $option.attr('selected', 1);
                    }

                    $selector.append($option);
                });
            }

        },

        getAvailableIssues: function(membershipIndex){
            var groups = this.getAvailableGroups(membershipIndex);

            var availableIssues = _.reduce(groups, function(memo, groupInstance){
                if (groupInstance.membership[membershipIndex]) {
                    return memo.concat(groupInstance.issues);
                }else{
                    return memo
                }
            }, []);

            return _.uniq(availableIssues, false, function(issueInstance){
                return issueInstance.id;
            });
        },

        renderIssuesSelectors: function () {
            var availableIssues;
            var availableModeratorIssues = this.getAvailableIssues(0); // 0 - moderator
            var availableMemberIssues = this.getAvailableIssues(1);    // 1 - member
            var availableFollowerIssues = this.getAvailableIssues(2);  // 2 - follower

            for (var key in this.issueSelectors) {
                var $selector = this.issueSelectors[key];
                $selector.empty();

                if ($selector.selector.indexOf('moderator') > -1) {
                    availableIssues = availableModeratorIssues;
                }else if ($selector.selector.indexOf('member') > -1) {
                    availableIssues = availableMemberIssues;
                }else {
                    availableIssues = availableFollowerIssues;
                }

                availableIssues.forEach(function(issueInstance){
                    var $option = $('<option value="' + issueInstance.id + '">' + issueInstance.name + '</option>');

                    if ($selector.selector.indexOf('moderator') > -1 && issueInstance.membership[0]) {
                        $option.attr('selected', 1);
                    }
                    if ($selector.selector.indexOf('member') > -1 && issueInstance.membership[1]) {
                        $option.attr('selected', 1);
                    }
                    if ($selector.selector.indexOf('follower') > -1 && issueInstance.membership[2]) {
                        $option.attr('selected', 1);
                    }

                    $selector.append($option);
                });
            }
        },

        renderSelectors: function(){

            console.log(this.state.themes[0].membership[0]);

            this.renderThemesSelectors();
            this.renderGroupsSelectors();
            this.renderIssuesSelectors();
        },

        themeChange: function($selector){
            var selectedValues = $selector.val();
            var selectorName = $selector[0].name;
            var previuosSelectedState = [];

            var membershipIndex = 0;

            if (selectorName.indexOf('member') > -1) {
                membershipIndex = 1;
            }else if (selectorName.indexOf('follower') > -1) {
                membershipIndex = 2;
            }

            this.state.themes.forEach(function(themeInstance){
                if (themeInstance.membership[membershipIndex]){
                    previuosSelectedState.push(themeInstance.id)
                }
            });

            var addedThemes = _.difference(selectedValues, previuosSelectedState);
            var removedThemes = _.difference(previuosSelectedState, selectedValues);

            this.state.themes.forEach(function(themeInstance){
                if (addedThemes.indexOf(themeInstance.id) > -1){
                    themeInstance.membership[membershipIndex] = true;
                    if (membershipIndex == 1) {
                        themeInstance.membership[2] = true;
                    }

                    themeInstance.groups.forEach(function(groupInstance){
                        groupInstance.membership[membershipIndex] = true;
                        if (membershipIndex == 1) {
                            groupInstance.membership[2] = true;
                        }

                        groupInstance.issues.forEach(function(issueInstance){
                            issueInstance.membership[membershipIndex] = true;
                            if (membershipIndex == 1) {
                                issueInstance.membership[2] = true;
                            }

                        });
                    });
                }

                if (removedThemes.indexOf(themeInstance.id) > -1){
                    themeInstance.membership[membershipIndex] = false;
                    if (membershipIndex == 1) {
                        themeInstance.membership[2] = false;
                    }

                    themeInstance.groups.forEach(function(groupInstance){
                        groupInstance.membership[membershipIndex] = false;
                        if (membershipIndex == 1) {
                            groupInstance.membership[2] = false;
                        }

                        groupInstance.issues.forEach(function(issueInstance){
                            issueInstance.membership[membershipIndex] = false;
                            if (membershipIndex == 1) {
                                issueInstance.membership[2] = false;
                            }
                        });
                    });
                }
            });
            return this.renderSelectors();
        },

        groupChange: function($selector){
            var selectedValues = $selector.val();
            var selectorName = $selector[0].name;
            var previuosSelectedState = [];

            var membershipIndex = 0;

            if (selectorName.indexOf('member') > -1) {
                membershipIndex = 1;
            }else if (selectorName.indexOf('follower') > -1) {
                membershipIndex = 2;
            }

            this.state.themes.forEach(function(themeInstance){
                themeInstance.groups.forEach(function(groupInstance) {
                    if (groupInstance.membership[membershipIndex]) {
                        previuosSelectedState.push(groupInstance.id);
                    }
                });
            });

            var addedGroups = _.difference(selectedValues, previuosSelectedState);
            var removedGroups = _.difference(previuosSelectedState, selectedValues);

            this.state.themes.forEach(function(themeInstance){
                themeInstance.groups.forEach(function(groupInstance){
                    if (addedGroups.indexOf(groupInstance.id) > -1){
                        groupInstance.membership[membershipIndex] = true;

                        groupInstance.issues.forEach(function(issueInstance){
                            issueInstance.membership[membershipIndex] = true;
                        });
                    }

                    if (removedGroups.indexOf(groupInstance.id) > -1){
                        groupInstance.membership[membershipIndex] = false;

                        groupInstance.issues.forEach(function(issueInstance){
                            issueInstance.membership[membershipIndex] = false;
                        });
                    }
                });
            });

            return this.renderSelectors();
        },

        selectorOnChange: function(e){
            var $selector = $(e.target);
            var selectorName = $selector[0].name;
            if (selectorName.indexOf('theme') > -1) {
                return this.themeChange($selector);
            }else if (selectorName.indexOf('group') > -1) {
                return this.groupChange($selector);
            }else{

            }
        }
    };

    ARWidget.init();

}(jQuery);
