/**
 * Created by yuri on 14.12.16.
 */
webshims.setOptions('waitReady', false);
webshims.setOptions('forms-ext', {types: 'date'});
webshims.polyfill('forms forms-ext');

$(function(){
  $( "#id_paginate_by" ).change(function() {
    var $form = $("form#search-list-filter");
    var paginateFormInput = $form.find('input[name="paginate_by"]')[0];

    if (!paginateFormInput) {
      $('<input />').attr('type', 'hidden')
          .attr('name', "paginate_by")
          .attr('value', $(this).val())
          .appendTo($form);
    }else{
      paginateFormInput.val($(this).val());
    }

    $form.submit();
  });
});
