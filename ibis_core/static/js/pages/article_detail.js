/**
 * Created by yuri on 08.11.16.
 */


+function ($) {
    $("#id_paginate_by").change(function () {
        $("form#articles-table").submit();
    });

    $("#id_status").change(function () {
        var $form = $("form#status-change");
        var statusInput = $form.find('input[name="status"]')[0];

        if (!statusInput) {
            $('<input />').attr('type', 'hidden')
                .attr('name', "status")
                .attr('value', $(this).val())
                .appendTo($form);
        } else {
            statusInput.val($(this).val());
        }

        $form.submit();
    });

}(jQuery);
