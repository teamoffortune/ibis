+function ($) {
    var articleTemlate = twig({data: $('#id-articles-template').html()});
    var tqResultWrapper = $('#id-tq-result-wrapper');

    $('#id-btn-test-query').on('click', function(){
        var main_query_text = $('#id_main_query').val().trim();
        var all_words = $('#id_all_words').val().trim();
        var exact_phrase = $('#id_exact_phrase').val().trim();
        var omit_words = $('#id_omit_words').val().trim();
        $.get(
            '/api/v1/testquery',
            {
                q: main_query_text,
                all_words: all_words,
                exact_phrase: exact_phrase,
                omit_words:omit_words
            }
        ).done(function(data){
                console.log(data);
                if (data['items'].length) {
                    renderedArticles = articleTemlate.render({articles:data['items']});
                    tqResultWrapper.empty().html(renderedArticles);
                }
            }
        )
    });

    // Activates Bootstrap Multiselect
    $(document).ready(function() {
        $('#source-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: false,
            disableIfEmpty: true,
            buttonWidth: '100%',
            dropRight: true
        });


    });
}(jQuery);
