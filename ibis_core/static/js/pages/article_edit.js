/**
 * Created by yuri on 11.12.16.
 */

+ function ($) {

    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }

    $('#id_tags').tagsinput({
        tagClass: 'label label-success big'
    });

    $('#id_tags').on('itemRemoved', function(event) {
        event.preventDefault();
        $.ajax({
            type: "GET",
            url: "~update",
            contentType: "application/json; charset=utf-8",
            // dataType: "json",
            data: {"remove_tag": event.item},
            success: function (result) {
                console.log(result.msg);
            }
        })
    });

    var $elt = $('#id_tags').tagsinput('input');

    $elt
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        source: function( request, response ) {
          $.getJSON( "~update", {
            term: extractLast( request.term )
          }, response );
        },
        search: function() {
          // custom minLength
          var term = extractLast( this.value );
          if ( term.length < 2 ) {
            return false;
          }
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.label );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( "" );
          return false;
        }
      });

    $(document).ready(function() {
        // render the template
        var locationsTable = $('#locations');
        var locationsHTML = twig({data: $('#locations-row-template').html()});
        if (map_items.length) {
            rows = locationsHTML.render({locations: map_items[0].coord});
            locationsTable.empty().html(rows);
        } else {
            rows = locationsHTML.render({locations: []});
            locationsTable.empty().html(rows);
        }
    });
}(jQuery);
