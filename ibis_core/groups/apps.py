from django.apps import AppConfig


class GroupsConfig(AppConfig):
    name = 'ibis_core.groups'
