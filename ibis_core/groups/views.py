from django.contrib.auth.mixins import LoginRequiredMixin, \
    PermissionRequiredMixin
from django import http
from django.core.urlresolvers import reverse
from django.db import transaction
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views import View
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView

from ibis_core.common.constants import PUBLIC
from ibis_core.common.mixins import UserGroupModeratorPermission, BaseModeratorMixin
from ibis_core.common.utils import get_groups_list_for_user, get_issues_list_for_user
from ibis_core.digest.models import DailyDigest
from ibis_core.groups.models import Group, GroupUsers
from ibis_core.issues.models import Issue, IssueGroup
from ibis_core.themes.models import Theme
from ibis_core.users.models import User


def get_user_allowed_themes(user):
    """
    Retuns themes list where user is moderator or follower
    :param user:
    :return: themes queryset
    """
    return Theme.objects.filter(members__user_id=user.id)


class GroupCreateView(LoginRequiredMixin, BaseModeratorMixin, CreateView):
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = 'groups.add_group'
    model = Group
    fields = ['name', 'access']

    def is_theme_moderator(self, **kwargs):
        theme_slug = self.kwargs.get('slug')
        theme_instance = get_object_or_404(Theme, slug=theme_slug)

        if theme_instance is not None:
            return self.request.user.themes.filter(
                is_moderator=True,
                theme_id=theme_instance.id
            ).count() == 1
        else:
            return False

    def is_moderator(self, **kwargs):
        # If slug in kwargs we suppose that group is created from theme page
        theme_slug = self.kwargs.get('slug')
        if theme_slug is not None:
            return self.is_theme_moderator(**kwargs)

        return True

    def form_valid(self, form):
        # We should add the user as group author and moderator
        self.object = form.save()
        group_author = GroupUsers(
            group=self.object,
            user=self.request.user,
            is_author=True,
            is_moderator=True
        )
        group_author.save()

        # Save group to selected themes if we just create new group (not from theme form)
        selected_themes = self.request.POST.getlist('group_theme')
        if len(selected_themes):
            [self.object.theme.add(theme_instance) for theme_instance in Theme.objects.filter(id__in=selected_themes)]

        # Save group to theme if we create it from theme page
        if self.slug_url_kwarg in self.kwargs:
            # Here we create group for theme
            # if slug in url we suppose that group created from theme form
            theme_instance = get_object_or_404(
                Theme,
                slug=self.kwargs.get(self.slug_url_kwarg)
            )

            self.object.save()
            self.object.theme.add(theme_instance)

        if self.object.access == PUBLIC:
            self.object.create_event(actor=self.request.user)

        return super(GroupCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        """
        Get the context for this view.
        """
        context = {}
        if self.slug_field not in self.kwargs:
            user = self.request.user
            context['themes'] = get_user_allowed_themes(user)

        context.update(kwargs)
        return super(GroupCreateView, self).get_context_data(**context)

    def get_success_url(self):
        if self.slug_url_kwarg in self.kwargs:
            theme_instance = get_object_or_404(
                Theme,
                slug=self.kwargs.get(self.slug_url_kwarg)
            )
            return reverse(
                'themes:detail',
                kwargs={'slug': theme_instance.slug}
            )

        # Sends the user back to the groups list
        return reverse('groups:list')


class GroupAddExistingIssueView(LoginRequiredMixin, UserGroupModeratorPermission, FormView):
    permission_required = 'groups.change_group'
    permission_denied_message = 'You have no permission to add issue to the group.'
    raise_exception = True
    template_name = 'issues/issues_select_list.html'

    def __init__(self):
        super(GroupAddExistingIssueView, self).__init__()
        self.group_instance = None

    def get_group_instance(self):
        if self.group_instance is not None:
            return self.group_instance
        slug_field = self.kwargs.get('slug')
        return get_object_or_404(Group, slug=slug_field)

    def get_queryset(self):
        """
        We should compose list of issues to select except existed group issues
        """
        self.group_instance = self.get_group_instance()
        exclude_list = self.group_instance.issues.values_list(
            'issue_id',
            flat=True
        )
        return Issue.objects.exclude(id__in=exclude_list)

    def get_context_data(self, **kwargs):
        """
        Get the context for this view.
        """
        queryset = self.get_queryset()
        context = {'paginator': None, 'page_obj': None, 'is_paginated': False,
                   'object_list': queryset, 'issues_list': queryset}

        context.update(kwargs)
        if 'view' not in kwargs:
            context['view'] = self
        return context

    def get_success_url(self):
        return reverse(
            'groups:detail',
            kwargs={'slug': self.get_group_instance().slug}
        )

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        issues_id_list = self.request.POST.getlist('_selected_issues', None)
        self.group_instance = self.get_group_instance()
        if self.group_instance and issues_id_list and len(issues_id_list):
            bulk_list = [
                    IssueGroup(
                        group_id=self.group_instance.id, issue_id=issue_id
                    ) for issue_id in issues_id_list
                ]
            IssueGroup.objects.bulk_create(bulk_list)

        return HttpResponseRedirect(self.get_success_url())


class GroupUpdateView(LoginRequiredMixin, UserGroupModeratorPermission,
                      UpdateView):
    template_name_suffix = '_update_form'
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = 'groups.change_group'
    model = Group
    fields = ['name']

    def form_valid(self, form):
        self.object = form.save()

        # Save group to selected themes if we just create new group (not from theme form)
        selected_themes = self.request.POST.getlist('group_theme')
        existed_themes = [str(id) for id in self.object.theme.all().values_list('id', flat=True)]

        themes_to_add = set(selected_themes) - set(existed_themes)
        themes_to_del = set(existed_themes) - set(selected_themes)

        if len(themes_to_del):
            [self.object.theme.remove(theme) for theme in Theme.objects.filter(id__in=themes_to_del)]

        if len(selected_themes):
            [self.object.theme.add(theme) for theme in Theme.objects.filter(id__in=themes_to_add)]

        return super(GroupUpdateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        """
        Get the context for this view.
        """
        context = super(GroupUpdateView, self).get_context_data(**kwargs)
        user = self.request.user
        context['themes'] = get_user_allowed_themes(user)
        context['selected_themes'] = context['object'].theme.all().values_list('id', flat=True)
        return context

    def get_success_url(self):
        return reverse('groups:list')


class GroupListView(LoginRequiredMixin, ListView):
    model = Group

    def get_queryset(self):
        # We should return groups where user is member
        if self.request.user.is_superuser:
            qs = super(GroupListView, self).get_queryset()
        else:
            qs = get_groups_list_for_user(self.request.user)

        if 'q' in self.request.GET:
            q_param = self.request.GET.get('q')
            qs = qs.filter(name__icontains=q_param)

        return qs

    def get_context_data(self, **kwargs):
        context = super(GroupListView, self).get_context_data(**kwargs)

        context['follower_list'] = GroupUsers.objects.filter(
            is_follower=True,
            user=self.request.user
        ).values_list('group_id', flat=True)

        return context


class GroupDetailView(LoginRequiredMixin, DetailView):
    model = Group

    def get_context_data(self, **kwargs):
        context = super(GroupDetailView, self).get_context_data(**kwargs)
        user = self.request.user
        group_slug = self.kwargs.get('slug')
        group_instance = get_object_or_404(Group, slug=group_slug)

        moderators = [group_user.user for group_user in group_instance.moderators]
        if user in moderators:
            context['digest'] = DailyDigest.objects.filter(group=group_instance)  # Digest list should be here

        context['group_issues'] = Issue.objects.filter(
            Q(groups__group__id=group_instance.id) & (Q(access=PUBLIC) | Q(members__user=self.request.user))
        ).distinct()
        context['users_issues'] = [issue.id for issue in get_issues_list_for_user(user=self.request.user)]

        return context


class GroupUsersAddView(LoginRequiredMixin, UserGroupModeratorPermission,
                        CreateView):
    """
    CBV for adding selected users to group members
    """
    permission_required = 'groups.add_groupusers'
    permission_denied_message = 'You have no permission to add group member.'
    model = GroupUsers
    raise_exception = True
    template_name = 'users/user_select_list.html'
    fields = ['is_moderator']

    def __init__(self):
        super(GroupUsersAddView, self).__init__()
        self.group_instance = None

    def get_queryset(self):
        """
        We should compose list of user to select except existed group members
        """
        group_slug = self.kwargs.get('slug')
        self.group_instance = get_object_or_404(Group, slug=group_slug)
        exclude_list = self.group_instance.members.values_list(
            'user_id',
            flat=True
        )
        return User.objects.exclude(id__in=exclude_list)

    def get_context_data(self, **kwargs):
        """
        Get the context for this view.
        """
        queryset = self.get_queryset()
        context = {'paginator': None, 'page_obj': None, 'is_paginated': False,
                   'object_list': queryset, 'user_list': queryset}

        context.update(kwargs)
        return super(GroupUsersAddView, self).get_context_data(**context)

    def get_group_instance(self):
        group_slug = self.kwargs.get('slug')
        return get_object_or_404(Group, slug=group_slug)

    def get_success_url(self):
        if self.group_instance is None:
            self.group_instance = self.get_group_instance()
        return reverse(
            'groups:detail',
            kwargs={'slug': self.group_instance.slug}
        )

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        users_id_list = self.request.POST.getlist('_selected_users', None)
        self.group_instance = self.get_group_instance()
        if self.group_instance and users_id_list and len(users_id_list):
            bulk_list = [
                GroupUsers(
                    group_id=self.group_instance.id, user_id=user_id
                ) for user_id in users_id_list
                ]
            GroupUsers.objects.bulk_create(bulk_list)
        return HttpResponseRedirect(self.get_success_url())


class GroupUserToModerator(LoginRequiredMixin, UserGroupModeratorPermission,
                           UpdateView):
    permission_denied_message = "You doesn't have enough permissions."
    permission_required = 'groups.change_groupusers'
    model = GroupUsers
    fields = ['is_moderator']

    def get_success_url(self):
        groupuser_instance = self.get_object()
        return reverse(
            'groups:detail',
            kwargs={'slug': groupuser_instance.group.slug}
        )


class GroupUsersDeleteView(LoginRequiredMixin, UserGroupModeratorPermission,
                           DeleteView):
    permission_required = 'groups.delete_groupusers'
    permission_denied_message = 'You have no permission to remove member.'
    model = GroupUsers
    raise_exception = True

    def get_success_url(self):
        groupuser_instance = self.get_object()
        return reverse(
            'groups:detail',
            kwargs={'slug': groupuser_instance.group.slug}
        )


class GroupFollowView(LoginRequiredMixin, View):
    model = GroupUsers

    @transaction.atomic()
    def post(self, request, **kwargs):
        user = request.user
        group_instance = get_object_or_404(Group, slug=kwargs['slug'])

        if group_instance.access == PUBLIC:
            group_instance.add_follower(user)

        return http.HttpResponseRedirect(reverse('groups:list'))


class GroupUnfollowView(LoginRequiredMixin, View):
    model = GroupUsers

    @transaction.atomic()
    def post(self, request, **kwargs):
        user = request.user
        group_instance = get_object_or_404(Group, slug=kwargs['slug'])

        if group_instance.access == PUBLIC:
            try:
                group_user = GroupUsers.objects.get(user=user, group=group_instance)
                group_user.is_follower = False
                group_user.save()
            except GroupUsers.DoesNotExist:
                pass

        return http.HttpResponseRedirect(reverse('groups:list'))




