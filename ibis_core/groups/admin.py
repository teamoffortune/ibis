from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from ibis_core.groups.models import Group, GroupUsers


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ['name', 'modified']
    search_fields = ['name', 'author']


@admin.register(GroupUsers)
class GroupUsersAdmin(admin.ModelAdmin):
    list_display = ['group', 'user']
