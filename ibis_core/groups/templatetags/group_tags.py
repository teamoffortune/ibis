from django import template

register = template.Library()


@register.inclusion_tag('searches/templatetags/search_info.html')
def group_info(group):
    return {'themes': group.theme.all()}