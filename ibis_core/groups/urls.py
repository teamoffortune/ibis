# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from ibis_core.groups.views import GroupAddExistingIssueView
from . import views
from ibis_core.issues.views import IssueCreateView

urlpatterns = [
    # URL pattern for the GroupListView
    url(
        regex=r'^$',
        view=views.GroupListView.as_view(),
        name='list'
    ),

    # URL pattern for the CreateGroup
    url(
        regex=r'^~new/$',
        view=views.GroupCreateView.as_view(),
        name='create'
    ),

    # URL pattern for the GroupDetailView
    url(
        regex=r'^(?P<slug>[\w-]+)$',
        view=views.GroupDetailView.as_view(),
        name='detail'
    ),

    # URL pattern for the GroupUpdateView
    url(
        regex=r'^(?P<slug>[\w-]+)/~update$',
        view=views.GroupUpdateView.as_view(),
        name='update'
    ),

    # Urls for group issues
    url(
        regex=r'^(?P<slug>[\w-]+)/~add_issue/$',
        view=IssueCreateView.as_view(),
        name='add_group_issue'
    ),

    url(
        regex=r'^(?P<slug>[\w-]+)/~add_existing_issue/$',
        view=GroupAddExistingIssueView.as_view(),
        name='add_group_existing_issue'
    ),

    # Urls for user management inside group
    url(
        regex=r'^(?P<slug>[\w-]+)/~add_members/$',
        view=views.GroupUsersAddView.as_view(),
        name='add_group_member_list'
    ),

    url(
        regex=r'^(?P<slug>[\w-]+)/~to_moderator/(?P<pk>[0-9]+)$',
        view=views.GroupUserToModerator.as_view(),
        name='to_moderator'
    ),

    url(
        regex=r'^(?P<slug>[\w-]+)/~remove_member/(?P<pk>[0-9]+)$',
        view=views.GroupUsersDeleteView.as_view(),
        name='remove_group_member'
    ),

    url(
        regex=r'^(?P<slug>[\w-]+)/~follow/$',
        view=views.GroupFollowView.as_view(),
        name='group_follow'
    ),

    url(
        regex=r'^(?P<slug>[\w-]+)/~unfollow/$',
        view=views.GroupUnfollowView.as_view(),
        name='group_unfollow'
    ),

]
