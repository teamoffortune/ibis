from django.conf import settings
from django.db import models

from ibis_core.activity.models import EventMixin
from ibis_core.common.constants import PUBLIC
from ibis_core.common.model_mixins import ModelAccessMixin
from ibis_core.common.models import AbstractNamedModel


class GroupUsers(models.Model):
    group = models.ForeignKey('Group', related_name='members')
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='themes_groups'
    )
    is_author = models.BooleanField(default=False)
    is_moderator = models.BooleanField(default=False)
    is_follower = models.BooleanField(default=False)


class Group(AbstractNamedModel, ModelAccessMixin, EventMixin):

    theme = models.ManyToManyField('themes.Theme', related_name='groups', blank=True)

    users = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        through=GroupUsers,
        through_fields=('group', 'user')
    )

    @property
    def moderators(self):
        return self.members.filter(is_moderator=True)

    @property
    def simple_members(self):
        return self.members.filter(is_author=False, is_moderator=False)

    @models.permalink
    def get_absolute_url(self):
        return 'groups:detail', (), {'slug': self.slug}

    def get_activity_followers(self):
        followers = [user_id for user_id in self.members.values_list('user_id', flat=True)]
        if self.access == PUBLIC:
            [followers.extend(theme.get_activity_followers()) for theme in self.theme.all()]

        return list(set(followers))

    def add_follower(self, user):
        if self.access == PUBLIC:
            try:
                group_user = GroupUsers.objects.get(user=user, group=self)
                group_user.is_follower = True
                group_user.save()
            except GroupUsers.DoesNotExist:
                GroupUsers.objects.create(user=user, group=self, is_follower=True)

            for theme in self.theme.all():
                theme.add_follower(user)



