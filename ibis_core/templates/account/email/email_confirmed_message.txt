{% load account %}{% user_display user as user_display %}{% load i18n %}{% autoescape off %}{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}

Dear {{user}}.

You are now a member of the biosecurity intelligence community at {{site_name}}.
You can now proceed to login at <a href='{{site}}/accounts/login/'>{{site}}/accounts/login/</a>
Once you login you will be able to follow biosecurity related Groups and Issues and receive the latest news on those topics.
If you should have any questions please feel free to contact us using the contact form on the site.

{% endblocktrans %}{% endautoescape %}
{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}Thank you from {{ site_name }}!
{{ site_domain }}{% endblocktrans %}
