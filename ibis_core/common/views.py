import json

from django.db.models import Q
from django.utils.text import Truncator
from django.views import View
from django.views.generic import TemplateView
from math import ceil

from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from ibis_core.common.constants import PUBLIC
from ibis_core.common.serializers import UMThemeSerializer
from ibis_core.common.utils import get_groups_list_for_user, get_issues_list_for_user, get_searches_list_for_user, \
    get_articles_list_for_user
from ibis_core.themes.models import Theme
from ibis_core.users.models import User


def global_search_request(q_param='', user=None, return_qs=False, limit=100, offset=0, suggested_mode=False):
    """
    Perform search among all IBIS entities
    """

    na_offset = None
    na_limit = None
    if suggested_mode:
        na_offset = 0
        na_limit = 5

    if not user or q_param == '':
        return None

    themes_qs = Theme.objects.filter(members__user=user).filter(Q(name__icontains=q_param))[na_offset:na_limit]
    themes_list = [{'name': theme.name, 'url': theme.get_absolute_url()} for theme in themes_qs]

    groups_qs = get_groups_list_for_user(user).filter(name__icontains=q_param)[na_offset:na_limit]
    groups_list = [{'name': group.name, 'url': group.get_absolute_url()}
                   for group in groups_qs]

    issues_qs = get_issues_list_for_user(user, q_param, limit=na_limit)
    issues_list = [{'name': issue.name, 'url': issue.get_absolute_url()} for issue in issues_qs]

    searches_qs = get_searches_list_for_user(user, q_param, limit=na_limit, offset=na_offset)
    searches_list = [{'name': search.name, 'url': search.get_absolute_url()} for search in searches_qs]

    articles_qs, total = get_articles_list_for_user(user, q_param, offset=offset, limit=limit)
    articles_list = [{
                         'name': article.title,
                         'url': article.get_absolute_url(),
                         'image_url': article.top_image_path,
                         'body': Truncator(article.translated_body).words(100, truncate=' ...')
                     } for article in articles_qs]

    if return_qs:
        return dict(
            themes=themes_qs,
            groups=groups_qs,
            issues=issues_qs,
            searches=searches_qs,
            articles=articles_qs,
            total=total
        )

    return dict(
        themes=themes_list,
        groups=groups_list,
        issues=issues_list,
        searches=searches_list,
        articles=articles_list
    )


class SuggestView(APIView):

    def get(self, request):
        if 'q' in request.GET:
            q_param = request.GET.get('q')

            search_result = global_search_request(q_param=q_param, user=request.user, offset=0, limit=5,
                                                  suggested_mode=True)

            return Response(search_result)

        return Response({'resp': 'ok'})


class GlobalSearchView(TemplateView):
    template_name = 'common/global_search.html'
    paginate_by = 25

    def get_context_data(self, **kwargs):
        context = super(GlobalSearchView, self).get_context_data(**kwargs)

        request = self.request

        paginate_by = request.GET.get('paginate_by', self.paginate_by)
        page = request.GET.get('page', 1)

        try:
            page = int(page)
        except ValueError:
            page = 1

        try:
            paginate_by = int(paginate_by)
        except ValueError:
            paginate_by = self.paginate_by

        if 'q' in request.GET:
            q_param = request.GET.get('q')

            if page == 0:
                page = 1

            offset = (page - 1) * paginate_by
            limit = paginate_by - 1

            search_result = global_search_request(
                q_param=q_param,
                user=request.user,
                offset=offset,
                limit=limit,
                return_qs=True
            )

            total = search_result['total']

            num_pages = int(ceil(total / float(paginate_by)))

            page_obj = dict(
                number=page,
                has_previous=page > 1,
                has_next=page < total,
                previous_page_number=max(1, page - 1),
                next_page_number=min(num_pages, page + 1)
            )

            context['paginator'] = dict(num_pages=num_pages)
            context['num_pages'] = num_pages
            context['page_obj'] = page_obj
            context['is_paginated'] = page_obj['has_previous'] or page_obj['has_next']

            context.update(search_result)

        return context


class ThemeChidlrensView(RetrieveAPIView):
    queryset = Theme.objects.all()
    serializer_class = UMThemeSerializer
    lookup_field = 'slug'


class ThemeListViewAPI(ListAPIView):
    queryset = Theme.objects.all()
    serializer_class = UMThemeSerializer


def get_selected_values_for_user(user):
    res = dict()
    res['moderator_themes'] = user.themes.filter(is_moderator=True).values_list('theme', flat=True)
    res['moderator_groups'] = user.themes_groups.filter(is_moderator=True).values_list('group', flat=True)
    res['moderator_issues'] = user.issues.filter(is_moderator=True).values_list('issue', flat=True)

    res['member_themes'] = user.themes.filter(is_moderator=False).values_list('theme', flat=True)
    res['member_groups'] = user.themes_groups.filter(is_moderator=False).values_list('group', flat=True)
    res['member_issues'] = user.issues.filter(is_moderator=False).values_list('issue', flat=True)

    res['follower_themes'] = user.themes.filter(
        is_follower=True
    ).values_list('theme', flat=True)
    res['follower_groups'] = user.themes_groups.filter(
        is_follower=True
    ).values_list('group', flat=True)
    res['follower_issues'] = user.issues.filter(
        is_follower=True
    ).values_list('issue', flat=True)

    return res


class UserManagementAPI(APIView):

    def get(self, request, format=None):
        user = request.user

        managed_user_id = request.GET.get('managed_user', None)

        managed_user = None
        if managed_user_id:
            try:
                managed_user = User.objects.get(pk=managed_user_id)
            except User.DoesNotExist:
                pass

        if user.is_ibis_superadmin:
            theme_list = Theme.objects.all()
        else:
            theme_list = Theme.objects.filter(
                members__user=user).all()

        selected_vals = dict()
        if managed_user:
            selected_vals = get_selected_values_for_user(managed_user)

        initial_serializer = UMThemeSerializer(theme_list, many=True)

        result = dict(
            data=initial_serializer.data,
            selected=selected_vals
        )

        return Response(result)
