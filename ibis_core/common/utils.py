import datetime
from django.db.models import Q
from django.db import connection

from ibis_core.articles.models import Article
from ibis_core.common.constants import PUBLIC
from ibis_core.groups.models import Group
from ibis_core.issues.models import Issue
from ibis_core.searches.models import Search


def get_groups_by_theme_user(user, theme):
    return Group.objects.filter(Q(theme=theme) & (Q(access=PUBLIC) | Q(members__user=user))).distinct()


def get_groups_list_for_user(user):
    return Group.objects.filter(Q(members__user=user) | Q(theme__members__user=user)).distinct()


def get_issues_list_for_user(user, search_query='', limit=0, offset=0):
    query_text = """
          SELECT * FROM issues_issue WHERE id IN
            (SELECT id FROM issues_issue
             WHERE access = 'public' and id in (SELECT issue_id FROM issues_issuegroup WHERE group_id in (
                SELECT group_id FROM ibis.public.groups_group_theme
                WHERE theme_id IN (
                  SELECT theme_id FROM themes_themeusers WHERE user_id = %s)
                UNION
                SELECT group_id FROM ibis.public.groups_groupusers
                WHERE user_id = %s
              ))
              UNION SELECT issue_id FROM issues_issueusers WHERE user_id = %s
            )
    """

    if len(search_query.strip()):
        query_text += """
        AND name ILIKE '%%""" + search_query.strip() + """%%'
        """

    query_text += """
            ORDER BY created DESC
        """

    if limit:
        query_text += """
                LIMIT """ + str(limit) + """
                """

    if offset:
        query_text += """
                        OFFSET """ + str(offset) + """
                        """



    result_query = Issue.objects.raw(query_text, [user.id, user.id, user.id])
    return result_query


def get_searches_list_for_user(user, search_query='', limit=0, offset=0):

    query_text = """
                    SELECT * FROM searches_search
                    WHERE (author_id = %s OR
                    id IN (
                      SELECT search_id FROM searches_search_issues
                      WHERE issue_id IN (
                        SELECT issue_id FROM issues_issuegroup WHERE group_id in (
                            SELECT group_id FROM ibis.public.groups_group_theme
                            WHERE theme_id IN (
                              SELECT theme_id FROM themes_themeusers WHERE user_id = %s)
                            UNION
                            SELECT group_id FROM ibis.public.groups_groupusers
                            WHERE user_id = %s
                          )
                          UNION SELECT issue_id FROM issues_issueusers WHERE user_id = %s
                      )
                    ))

                """

    if len(search_query.strip()):
        query_text += """
        AND (name ILIKE '%%""" + search_query.strip() + """%%' OR main_query ILIKE '%%""" + \
                      search_query.strip() + """%%')
        """

    query_text += """
        ORDER BY created DESC
    """

    if limit:
        query_text += """
                   LIMIT """ + str(limit) + """
                   """

    if offset:
        query_text += """
                           OFFSET """ + str(offset) + """
                           """

    result_query = Search.objects.raw(query_text, [user.id, user.id, user.id, user.id])

    return result_query


def get_articles_list_for_issue(issue, page=1, paginate_by=25):
    issue_articles = Article.objects.filter(
        Q(issue_articles=issue) | Q(search_query__issues=issue) & (
            Q(status='keep') | Q(status='promoted') | Q(status='alert'))
    ).order_by('-created')

    if page == 0:
        page = 1

    offset = (page - 1) * paginate_by
    limit = paginate_by - 1

    return issue_articles[offset:limit], len(issue_articles)


def get_searches_list_for_themes(themes_list):
    str_themes_list = str(themes_list).replace('[', '').replace(']', '')
    sq = Search.objects.raw("""
        SELECT * FROM ibis.public.searches_search
        WHERE id IN (
              SELECT search_id FROM ibis.public.searches_search_issues
              WHERE issue_id IN (
                SELECT issue_id FROM ibis.public.issues_issuegroup WHERE group_id in (
                    SELECT group_id FROM ibis.public.groups_group_theme
                    WHERE theme_id IN (
                      SELECT id FROM ibis.public.themes_theme
                      WHERE slug IN (""" + str_themes_list + """)
                    )
                  )
              )
            )
        ORDER BY created DESC
    """)

    return sq


def get_articles_list_for_theme(theme):
    return Search.objects.raw("""
        SELECT * FROM articles_article WHERE search_query_id IN (
            SELECT id FROM searches_search
            WHERE id IN (
                  SELECT search_id FROM searches_search_issues
                  WHERE issue_id IN (
                    SELECT issue_id FROM issues_issuegroup WHERE group_id in (
                        SELECT group_id FROM ibis.public.groups_group_theme
                        WHERE theme_id = %s
                      )
                  )
                )
        )
        ORDER BY created DESC
    """, [theme.id])


def get_articles_list_for_user(user, search_query='', limit=0, offset=0):
    query_text = """
        SELECT DISTINCT * FROM articles_article WHERE search_query_id IN (
          SELECT id
          FROM searches_search
          WHERE (author_id = %s OR
                id IN (
                  SELECT search_id
                  FROM searches_search_issues
                  WHERE issue_id IN (
                    SELECT issue_id
                    FROM issues_issuegroup
                    WHERE group_id IN (
                      SELECT group_id
                      FROM ibis.public.groups_group_theme
                      WHERE theme_id IN (
                        SELECT theme_id
                        FROM themes_themeusers
                        WHERE user_id = %s)
                      UNION
                      SELECT group_id
                      FROM ibis.public.groups_groupusers
                      WHERE user_id = %s
                    )
                    UNION SELECT issue_id
                          FROM issues_issueusers
                          WHERE user_id = %s
                  )
                )
        ))

    """

    params_list = [user.id, user.id, user.id, user.id]

    if len(search_query.strip()):
        query_text += """
                    AND
                    (to_tsvector('english', COALESCE("articles_article"."translated_title", '') ||
                    ' ' || COALESCE("articles_article"."translated_body", '')) @@ (plainto_tsquery('english', %s))=true)
                """
        params_list.append(search_query.strip())

    count_query = """
            SELECT COUNT(*) FROM (%s) as total_articles
        """ % query_text

    with connection.cursor() as cursor:
        cursor.execute(count_query, params_list)

        try:
            total = cursor.fetchone()[0]
        except Exception as e:
            total = 0


    query_text += """
           ORDER BY created DESC
       """


    if limit:
        query_text += """
                   LIMIT """ + str(limit) + """
                   """

    if offset:
        query_text += """
                   OFFSET """ + str(offset) + """
                   """

    result_query = Article.objects.raw(query_text, params_list)

    return result_query, total


def do_filtered_search(request, page=1, paginate_by=25, return_total=True):
    user_id = request.user.id

    search_query = request.GET.get('q', None)
    article_status = request.GET.getlist('article_status', None)
    article_channel = request.GET.getlist('channel', None)
    themes_list = request.GET.getlist('theme', None)
    last_days = request.GET.get('last_days', None)
    date_start = request.GET.get('date_start', None)
    date_stop = request.GET.get('date_stop', None)
    locations = None

    params_list = []

    query_text = """
                SELECT
                  "articles_article"."id",
                  "articles_article"."created",
                  "articles_article"."modified",
                  "articles_article"."access",
                  "articles_article"."article_url",
                  "articles_article"."source_language",
                  "articles_article"."translated_title",
                  "articles_article"."translated_body"
                FROM "articles_article"
            """

    if themes_list:
        str_themes_list = str(themes_list).replace('[', '').replace(']', '')

        query_text += """
            WHERE search_query_id IN (
              SELECT id
              FROM searches_search
              WHERE (id IN (
                      SELECT search_id
                      FROM searches_search_issues
                      WHERE issue_id IN (
                        SELECT issue_id
                        FROM issues_issuegroup
                        WHERE group_id IN (
                          SELECT group_id
                          FROM ibis.public.groups_group_theme
                          WHERE theme_id IN (
                                  SELECT id FROM ibis.public.themes_theme
                                  WHERE slug IN (%s)
                          )
                        )
                      )
                    )
            ))
        """

        params_list.append(str_themes_list)
    else:
        query_text += """
            WHERE search_query_id IN (
              SELECT id
              FROM searches_search
              WHERE (author_id = %s OR
                    id IN (
                      SELECT search_id
                      FROM searches_search_issues
                      WHERE issue_id IN (
                        SELECT issue_id
                        FROM issues_issuegroup
                        WHERE group_id IN (
                          SELECT group_id
                          FROM ibis.public.groups_group_theme
                          WHERE theme_id IN (
                            SELECT theme_id
                            FROM themes_themeusers
                            WHERE user_id = %s)
                          UNION
                          SELECT group_id
                          FROM ibis.public.groups_groupusers
                          WHERE user_id = %s
                        )
                        UNION SELECT issue_id
                              FROM issues_issueusers
                              WHERE user_id = %s
                      )
                    )
            ))
        """

        [params_list.append(user_id) for i in range(4)]


    if search_query:
        query_text += """
            AND
            (to_tsvector('english', COALESCE("articles_article"."translated_title", '') ||
            ' ' || COALESCE("articles_article"."translated_body", '')) @@ (plainto_tsquery('english', %s)) = true)
        """

        params_list.append(search_query)

    if article_status:
        str_status_list = str(article_status).replace('[', '').replace(']', '')
        query_text += """
            AND
            "articles_article"."status" IN (""" + str_status_list + """)
        """

    if article_channel:
        str_channels_list = str(article_channel).replace('[', '').replace(']', '')
        query_text += """
                    AND
                    "articles_article"."channel" IN (""" + str_channels_list + """)
                 """

    if last_days or date_start and date_stop:
        if date_start and date_stop:
            end_date = date_stop
            start_date = date_start
        else:
            try:
                last_days = int(last_days)
            except:
                # TODO: Remove this very dirty hack
                last_days = 1

            end_date = datetime.datetime.today().strftime("%Y-%m-%d")
            start_date = (datetime.datetime.today() - datetime.timedelta(days=int(last_days))).strftime("%Y-%m-%d")

        query_text += """
                    AND
                    "articles_article"."post_date_crawled" BETWEEN %s AND %s
                 """

        params_list.append(start_date)
        params_list.append(end_date)

    total = 0
    if return_total:
        count_query = """
            SELECT COUNT(*) FROM (%s) as total_articles
        """ % query_text

        with connection.cursor() as cursor:
            cursor.execute(count_query, params_list)

            try:
                total = cursor.fetchone()[0]
            except Exception as e:
                total = 0

    if page == 0:
        page = 1

    offset = (page - 1) * paginate_by
    limit = paginate_by - 1

    query_text += """
        ORDER BY "articles_article"."post_date_crawled" DESC
        OFFSET %s LIMIT %s
    """ % (str(offset), str(limit))

    sq = Article.objects.raw(query_text, params_list)

    return sq, total
