import re

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


def tag_validator(self):
    """
    Should raise a ValidationError when tag isn't match pattern
    :param self:
    :return:
    """
    error_tags = []
    for element in self:
        m = re.match("(^[A-Z]\d[A-Z]\d)", element)
        if not m:
            error_tags.append(element)
    if len(error_tags) > 0:
        raise ValidationError(
            _('The following tags are not valid %(error_tags)s.'),
            params={'error_tags': error_tags},
        )
