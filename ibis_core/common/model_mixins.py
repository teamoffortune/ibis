from django.db import models

from ibis_core.common.constants import PUBLIC, PROTECTED, PRIVATE


class ModelAccessMixin(models.Model):
    ACCESS_CHOICES = (
        (PUBLIC, 'Public'),
        # (PROTECTED, 'Protected'),
        (PRIVATE, 'Private')
    )

    DEFAULT_ACCESS_CHOICE = PUBLIC

    access = models.CharField(
        max_length=12,
        choices=ACCESS_CHOICES,
        default=DEFAULT_ACCESS_CHOICE
    )

    class Meta:
        abstract = True
