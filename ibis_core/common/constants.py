
PUBLIC = 'public'
PROTECTED = 'protected'
PRIVATE = 'private'

#  Channels and statuses are using in ibis_core.searches and ibis_core.articles
CHANNELS = (
    ('industry', 'Industry'),
    ('research', 'Research'),
    ('government', 'Government'),
    ('other', 'Other biointel'),
    ('search_engines', 'Search Engines'),
    ('social', 'Social'),
    ('general', 'General')
)
STATUSES = (
    ('keep', 'Keep'),
    ('alert', 'Alert'),
    ('promoted', 'Promoted'),
    ('trash', 'Trash'),
    ('raw', 'Raw')
)
