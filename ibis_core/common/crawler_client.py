import requests
from django.conf import settings


class CrawlerClient(object):
    def __init__(self, crawler_address=None):
        self.crawler_url = crawler_address or settings.CRAWLER_ADDRESS

    def post(self, url, data):
        return requests.post(
            self.crawler_url + url,
            data=data
        )

    def put(self, url, data):
        return requests.put(
            self.crawler_url + url,
            data=data
        )

    def get(self, url):
        return requests.get(
            self.crawler_url + url
        )

    def get_sources_list(self):
        response = self.get('source_list')
        return response.json()

    def get_search_types_list(self):
        response = self.get('search_types_list')
        return response.json()

    def add_search_query(self, data):
        response = self.post(
            url='search_query/',
            data=data
        )
        return response.json()

    def create_or_update_search_query(self, data):
        search_id = data['search_id']
        response = self.put(
            url='search_query/%s' % search_id,
            data=data
        )

        if response.status_code == 404:
            return self.add_search_query(data)

        return response.json()

    def get_search_articles(self, search_id):
        response = self.get(
            url='get_search_articles/%s' % search_id
        )
        return response.json()
