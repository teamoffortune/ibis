import requests
from django.conf import settings


class MailgunClient(object):
    """
    Client for manipulating with Mailgun API
    ref: https://documentation.mailgun.com/api_reference.html
    """
    def __init__(self):
        self.auth = 'key-072d83e1204d870ab54f20310a2552a4'
        self.api_base_url = 'https://api.mailgun.net/v3/endlessripples.com.au'
        self.from_email = settings.DEFAULT_FROM_EMAIL

    def post(self, endpoint, data):
        return requests.post(
            self.api_base_url + endpoint,
            auth=('api', self.auth),
            data=data
        )

    def send_message(self, subject, recipients, text='', html=''):
        data = {
            'from': self.from_email,
            'to': recipients,
            'subject': subject,
            'text': text,
            'html': html
        }
        self.post('/messages', data=data)


