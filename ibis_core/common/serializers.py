from rest_framework import serializers

from ibis_core.groups.models import Group
from ibis_core.issues.models import Issue, IssueGroup
from ibis_core.themes.models import Theme


class UMIssueGroupSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField(source='issue.id')
    name = serializers.ReadOnlyField(source='issue.name')

    class Meta:
        model = IssueGroup
        fields = ('id', 'name')


class UMGroupSerializer(serializers.ModelSerializer):
    issues = UMIssueGroupSerializer(many=True, read_only=True)

    class Meta:
        model = Group
        fields = ('id', 'name', 'issues')


class UMThemeSerializer(serializers.ModelSerializer):
    groups = UMGroupSerializer(many=True)

    class Meta:
        model = Theme
        fields = ('id', 'name', 'groups')
        read_only_fields = ('groups', )

