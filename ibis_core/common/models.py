import uuid

from django.core.exceptions import ValidationError
from django.db import models

from autoslug import AutoSlugField


class AbstractBaseModel(models.Model):
    """
    An abstract base class model that provides self updating 'created'
    and 'modified' fields to every model that inherits it.
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def are_fields_valid(self):
        try:
            self.full_clean()
        except ValidationError:
            return False
        return True

    class Meta:
        abstract = True


class AbstractNamedModel(AbstractBaseModel):
    """
    An abstract base class model that provides name and slug
    """
    name = models.CharField(max_length=200, db_index=True)
    slug = AutoSlugField(populate_from='name', always_update=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True
