import re


def untag(text):
    return re.sub(r'<[^>]*>', '', text)


def split_by_sentences(text, num_sentences=10):
    """
    This function splits the text to the groups that consist of num_sentences sentences.
    The split function is based on regular expression.
    :param text: The text to be split
    :param num_sentences: Number of sentences in every part of list
    """
    # Here the newline symbols are deleted and replaced by the space
    text = re.sub('\n', ' ', text)
    text = re.sub('”|“', '"', text)
    # Here the text is splitted by sentences and later it is grouped into groups each with num_sentences sentences
    splitted_text = [sen + text[text.find(sen) + len(sen):text.find(sen) + len(sen)+2]
                     if text[text.find(sen) + len(sen): text.find(sen) + len(sen)+2].endswith(' ')
                     # This check is used to know is the element after dot a space or not
                     else sen + text[text.find(sen) + len(sen):text.find(sen) + len(sen)+2]
                     if len(sen) > 0  # last element of split is usually an empty string, so we ned to exclude it
                     else ''  #
                     for sen in re.split(re.compile(r'[.!?]+[\t ")]'), text)  # splitting text by sentences
                     ]
    return [''.join(splitted_text[n:n+num_sentences])
            for n in range(len(splitted_text)) if n % num_sentences == 0]


def tag_p(splitted_text):
    """
    This function adds <p> </p> tags to the text that was previously splitted by sentences.
    :param splitted_text:
    :return: String with parts of texts, separated by the <p> tag
    """
    def add_symbols(text_list):
        for num in range(len(text_list)):
            if len(re.findall('"', text_list[num])) % 2 == 1:
                if re.split(r'"[^"]*"', text_list[num])[-1][-1] != '"':
                    text_list[num] += '"'
                    if num != len(text_list) - 1:
                        text_list[num + 1] = '"' + text_list[num + 1]
        return splitted_text
    sent_list = add_symbols(splitted_text)
    return '\n'.join('<p> ' + sens + '</p>' for sens in sent_list)
