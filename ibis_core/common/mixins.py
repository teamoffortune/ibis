from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.shortcuts import get_object_or_404

from ibis_core.digest.models import DailyDigest
from ibis_core.groups.models import Group
from ibis_core.issues.models import Issue
from ibis_core.themes.models import Theme


class BaseModeratorMixin(PermissionRequiredMixin):
    """
        Mixin that checks user moderator access rights
        """
    permission_denied_message = 'You have no permission.'

    def handle_no_permission(self):
        messages.warning(self.request, self.get_permission_denied_message())
        return HttpResponseRedirect(self.get_success_url())

    def is_moderator(self, **kwargs):
        raise NotImplementedError

    def dispatch(self, request, *args, **kwargs):
        if not (self.has_permission() and self.is_moderator(**kwargs)):
            return self.handle_no_permission()
        return super(PermissionRequiredMixin, self).dispatch(request, *args,
                                                             **kwargs)


class UserThemeModeratorPermission(BaseModeratorMixin):
    def is_moderator(self, **kwargs):
        theme_slug = self.kwargs.get('slug')
        theme_instance = get_object_or_404(Theme, slug=theme_slug)

        if theme_instance is not None:
            return self.request.user.themes.filter(
                is_moderator=True,
                theme_id=theme_instance.id
            ).count() == 1
        else:
            return False


class UserGroupModeratorPermission(BaseModeratorMixin):
    """
    Mixin that checks user moderator access rights
    """
    def is_moderator(self, **kwargs):
        group_slug = self.kwargs.get('slug')
        group_instance = get_object_or_404(Group, slug=group_slug)
        group_id = group_instance.id

        if group_id is not None:
            return self.request.user.themes_groups.filter(
                is_moderator=True,
                group_id=group_id
            ).count() == 1
        else:
            return False


class UserDigestModeratorPermission(BaseModeratorMixin):
    """
    Mixin that checks user moderator access rights for the DailyDigest model
    """
    def is_moderator(self, **kwargs):
        digest_id = self.kwargs.get('pk')
        digest_instance = get_object_or_404(DailyDigest, id=digest_id)

        moderators = digest_instance.moderators
        if digest_instance:
            return moderators.filter(is_moderator=True, user=self.request.user).count() == 1
        else:
            return False

    def handle_no_permission(self):
        return HttpResponseRedirect('/')


class UserGroupIssueModeratorPermission(UserGroupModeratorPermission):
    """
    Mixin that checks user moderator to create issue in group access rights
    """
    def is_moderator(self, **kwargs):
        # If slug in kwargs we suppose that issue created in group otherwise
        # the issue is not attached to group
        if 'slug' in kwargs:
            return super(UserGroupIssueModeratorPermission, self).\
                is_moderator()
        return True


class UserIssueSearchModeratorPermission(BaseModeratorMixin):
    """
    Mixin that checks user moderator to create search in issue access rights
    """
    def is_moderator(self, **kwargs):
        # If slug in kwargs we suppose that search created in issue otherwise
        # the search is not attached to issue
        if 'slug' in kwargs:
            issue_slug = kwargs.get('slug')
            issue_instance = get_object_or_404(Issue, slug=issue_slug)
            issue_id = issue_instance.id

            if issue_id is not None:
                return self.request.user.issues.filter(
                    is_moderator=True,
                    issue_id=issue_id
                ).count() == 1
            else:
                return False
        return True


class EditRightsPermision(BaseModeratorMixin):
    """
    Mixin that checks user moderator to edit object
    Object model should contain has_edit_rights(user) method
    """

    def get_permisson_object(self):
        return (hasattr(self, 'get_object') and self.get_object()
                or getattr(self, 'object', None))

    def is_moderator(self, **kwargs):
        self.object = self.get_permisson_object()

        # Check whether user can edit search
        if self.object is not None and self.object:
            user = self.request.user
            return self.object.has_edit_rights(user)

        return True


class UserSearchModeratorPermission(EditRightsPermision):
    """
    Mixin that checks user moderator to edit search
    """
    pass


class UserIssueModeratorPermission(EditRightsPermision):
    """
    Mixin that checks user moderator to edit issue
    """
    pass
