from django.apps import AppConfig


class FlatpagesIbisConfig(AppConfig):
    name = 'ibis_core.flatpages_ibis'
