from django.contrib import admin
from django.contrib.flatpages.models import FlatPage

# Note: we are renaming the original Admin and Form as we import them!
from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
from django.contrib.flatpages.admin import FlatpageForm as FlatpageFormOld

from django import forms
from django_summernote.widgets import SummernoteWidget


class FlatpageForm(FlatpageFormOld):
    content = forms.CharField(widget=SummernoteWidget())

    class Meta:
        model = FlatPage
        fields = ['title', 'content']


class FlatPageAdmin(FlatPageAdminOld):
    form = FlatpageForm


# We have to unregister the normal admin, and then reregister ours
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)
