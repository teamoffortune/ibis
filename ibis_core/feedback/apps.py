from django.apps import AppConfig


class FeedbackConfig(AppConfig):
    name = 'ibis_core.feedback'
    verbose_name = "Feedbacks"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        pass

