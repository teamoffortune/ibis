from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from ibis_core.feedback.models import Feedback


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    search_fields = ['email']

