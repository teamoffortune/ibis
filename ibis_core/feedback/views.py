from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.timezone import now
from django.views.generic import CreateView

from ibis_core.feedback.models import Feedback


class FeedbackCreateView(CreateView):
    model = Feedback
    fields = ['email', 'body']

    def form_valid(self, form):
        response = super(FeedbackCreateView, self).form_valid(form)
        if hasattr(settings, 'FEEDBACK_EMAILS'):
            cleaned_data = form.cleaned_data

            ctx = dict(
                email=cleaned_data['email'],
                body=cleaned_data['body'],
                browser_info=self.request.META.get('HTTP_USER_AGENT', 'was unable to get browser info'),
                user_ip_addr=self.request.META.get('HTTP_REMOTE_ADDR',
                                                   self.request.META.get(
                                                       'REMOTE_ADDR',
                                                       'was unable to get user ip address')),
                page=self.request.META.get('HTTP_REFERER', 'was unable to get page'),
                date=now(),
            )

            body_text = render_to_string('common/feedback_email_body.txt', ctx)
            try:
                send_mail(
                    u'Feedback received from {}.'.format(cleaned_data['email']),
                    body_text,
                    settings.DEFAULT_FROM_EMAIL,
                    settings.FEEDBACK_EMAILS,
                    fail_silently=False,
                )
            except Exception as e:
                pass
        return response

    def get_success_url(self):
        return '/'
