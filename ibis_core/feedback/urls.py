# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    # URL pattern for the CreateTheme
    url(
        regex=r'^~new/$',
        view=views.FeedbackCreateView.as_view(),
        name='create'
    ),
]
