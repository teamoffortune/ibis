from django.db import models

from ibis_core.common.models import AbstractBaseModel


class Feedback(AbstractBaseModel):

    email = models.EmailField(blank=True)
    body = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.email
