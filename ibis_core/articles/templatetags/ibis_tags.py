import datetime
from django import template
from django.utils import formats

register = template.Library()


@register.filter(expects_localtime=True, is_safe=False)
def date_format(value, arg):

    if value in (None, ''):
        return ''

    if isinstance(value, str):
        try:
            value = datetime.datetime.strptime(value, "%Y-%m-%d").date()
        except:
            return ''

    try:
        return formats.date_format(value, arg)
    except AttributeError:
        try:
            return format(value, arg)
        except AttributeError:
            return ''
