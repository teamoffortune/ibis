from datetime import datetime

from django.contrib.sites.models import Site
from django.db import models
import celery
from django.db import transaction
from django.urls import reverse
from django.utils.timezone import utc
from django.contrib.postgres.fields import JSONField, ArrayField

from ibis_core.articles.tasks import get_geo_entity_for_article
from ibis_core.common.model_mixins import ModelAccessMixin
from ibis_core.common.models import AbstractBaseModel
from ibis_core.common.validators import tag_validator
from ibis_core.digest.models import DailyDigest
from ibis_core.searches.models import Search


class Article(AbstractBaseModel, ModelAccessMixin):
    CHANNELS = (
        ('industry', 'Industry'),
        ('research', 'Research'),
        ('government', 'Government'),
        ('other', 'Other biointel'),
        ('search_engines', 'Search Engines'),
        ('social', 'Social'),
        ('general', 'General')
    )
    STATUSES = (
        ('keep', 'Keep'),
        ('alert', 'Alert'),
        ('promoted', 'Promoted'),
        ('trash', 'Trash'),
        ('raw', 'Raw')
    )

    # Next fields are getting from IBIS Crawler
    article_url = models.URLField(max_length=2048, unique=True)
    source_language = models.CharField(max_length=50, blank=True, null=True)
    title = models.CharField(max_length=1000, blank=True, db_index=True)
    translated_title = models.CharField(max_length=1000, blank=True)
    body = models.TextField(blank=True, null=True)
    translated_body = models.TextField(blank=True, null=True)
    authors = models.CharField(max_length=1000, blank=True, null=True)
    post_date_created = models.CharField(max_length=50, blank=True)
    post_date_crawled = models.DateTimeField(blank=True, null=True)
    translated = models.BooleanField(default=False)
    # Top image home url (from article node)
    top_image_url = models.URLField(max_length=1000, blank=True, null=True)
    # Path to image storage
    top_image_path = models.CharField(max_length=1000, blank=True, null=True)
    search_query = models.ForeignKey(Search, blank=True, null=True, related_name='articles')
    processed = models.BooleanField(default=False)

    # These fields are for internal using
    status = models.CharField(choices=STATUSES, max_length=50, blank=True, null=True, db_index=True)
    channel = models.CharField(choices=CHANNELS, max_length=50, blank=True, null=True, db_index=True)

    # Stores articles locations (lat, lng, place)
    locations = JSONField(blank=True, null=True)

    # Article tags
    tags = models.ManyToManyField('tagging.Tag', blank=True)

    # Fields from OLD IBIS
    domains = JSONField(blank=True, null=True)
    summary = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.article_url

    @models.permalink
    def get_absolute_url(self):
        return 'articles:detail', (), {'pk': self.id}

    @models.permalink
    def get_ulr_for_email_template(self):
        current_site = Site.objects.get_current().domain
        return str(current_site + self.get_absolute_url())

    # email_link = get_ulr_for_email_template

    def get_success_url(self):
        return reverse('articles:detail', kwargs={'pk': self.id})

    def has_edit_rights(self, user):
        return self.search_query.has_edit_rights(user)

    # def save(self, *args, **kwargs):
    #
    #     super(Article, self).save(*args, **kwargs)
    #
    #     if not self.status:
    #         self.set_article_status(self.search_query.source.split(', '))
    #
    #     if self.status == 'promoted' or self.status == 'alert':
    #         # Bind our article to the daily digest
    #         self.send_to_digest()

    def set_article_status(self, sources_list):

        search_engine_sources_list = ['google', 'google_cse', 'google_blogs', 'google_news',
                                      'google_scholar', 'bing', 'yandex']

        social_sources_list = ['facebook', 'linkedin']

        if len(set(search_engine_sources_list).intersection(sources_list)) != 0:
            self.status = 'raw'
            self.channel = 'search_engines'
        if len(set(social_sources_list).intersection(sources_list)) != 0:
            self.status = 'raw'
            self.channel = 'social'
        if self.search_query.type == 'rss':
            self.status = 'keep'
        if self.search_query.type == 'email':
            self.status = 'keep'
        return self.save()

    @transaction.atomic
    def send_to_digest(self):
        article_groups = []
        for issue in self.search_query.issues.all():
            for issue_group in issue.groups.all():
                article_groups.append(issue_group.group)
        digests = DailyDigest.objects.filter(group__in=article_groups, created__day=datetime.today().day)
        if len(digests) > 0:
            for digest in digests:
                digest.articles.add(self)
        else:
            for group in list(set(article_groups)):
                digest = DailyDigest.objects.create(group=group)
                # digest.save()
                digest.articles.add(self)
                # for member in group.members.all():
                #     if member.user.is_subscribed:
                #         digest.recipients.add(member.user)
