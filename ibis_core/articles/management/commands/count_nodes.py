import os, binascii
from os import walk
import pprint

import xmltodict
import json

# import django
from django.db import IntegrityError
from langdetect import detect
from langdetect.lang_detect_exception import LangDetectException

# sys.path.append('/webapps/ibis/ibis')
# from ibis_core import *
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')
# django.setup()

from bs4 import BeautifulSoup
# from crawl_engine.utils.articleDateExtractor import _extractFromHTMLTag as html_to_date
from ibis_core.articles.models import Article
from ibis_core.searches.models import Search
from ibis_core.tagging.models import Tag
from django.db import transaction
from django.utils.text import slugify
from django.core.management import BaseCommand

# log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Count XML nodes from particular XML dump. Only for internal using.'

    def add_arguments(self, parser):
        parser.add_argument('f', type=str)

    # @transaction.atomic
    def handle(self, *args, **options):
        # f = '/home/yuri/Downloads/new_full_article_dump/940019-1109969.xml'
        f = options['f']
        xml = open(f, 'r')
        self.stdout.write(self.style.SUCCESS("Reading from file %s ..." % f))
        soup = BeautifulSoup(xml, "lxml")
        self.stdout.write(self.style.SUCCESS("Soup is prepared. parsing nodes..."))
        nodes = soup.find_all('node')
        self.stdout.write(self.style.SUCCESS("%d NODES found" % len(nodes)))
