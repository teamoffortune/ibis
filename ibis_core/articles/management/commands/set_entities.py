import json
import time
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.db.models import Q
from autoslug.utils import slugify

from ibis_core.tagging.models import Tag

from ibis_core.common.geo_entities_extractor import convert_to_json
from ibis_core.common.paragraps_processor import untag, split_by_sentences, tag_p


class Command(BaseCommand):
    help = 'Sets geo entities and tags keywords to the articles'

    @transaction.atomic
    def handle(self, *args, **options):
        from ibis_core.articles.models import Article
        # Set articles with empty tags, locations,
        articles = Article.objects.filter(Q(tags=None) | Q(locations__isnull=True))[:20]

        counter = 0  # Count 10 articles per transaction
        articles_count = 0
        articles_updated = 0
        for article in articles:
            try:
                new_body = tag_p(split_by_sentences(untag(str(article.body))))
            except IndexError:
                new_body = None
            try:
                new_translated_body = tag_p(split_by_sentences(untag(str(article.translated_body))))
            except IndexError:
                new_translated_body = None

            if new_body and new_translated_body:
                article.body = new_body
                article.translated_body = new_translated_body
            # Call AlchemyAPI to provide an entities
            # They returned in JSON. So loads to the dict
            if new_translated_body:
                entities = json.loads(convert_to_json(new_translated_body))
                # Prepare locations data
                article.locations = parse_coordinates(entities['location'], entities['geo_entities'])
                # Get keyword tags and get_or_create Tag object by slug field.
                tags = list()
                # Check for tag in database or create one
                if entities['keywords'] and len(entities['keywords']) > 0:
                    self.stdout.write(self.style.SUCCESS("Keywords: %s" % entities['keywords']))
                    for tag_name in entities['keywords']:
                        obj, created = Tag.objects.get_or_create(
                            slug=slugify(tag_name),
                            defaults={'name': tag_name},
                        )
                        # Push created Tag object to the tags list
                        tags.append(obj)
                # Add keyword
                article.tags.add(*tags)
                article.save()
                articles_updated += 1
                sid = transaction.savepoint()
                if counter <= 4:
                    counter += 1
                else:
                    transaction.savepoint_commit(sid)
                    self.stdout.write(self.style.SUCCESS("Successfully SAVED %d articles to DB" % counter))
                    counter = 0
                self.stdout.write(self.style.SUCCESS('Successfully SET entities for article "%s"' % article.id))
            articles_count += 1

        self.stdout.write(
            self.style.SUCCESS(
                'Processed %d articles.\nUpdated %d articles.\nProcessing time: %s' % (articles_count, articles_updated, time.clock())))


def parse_coordinates(array_coords, array_geo_entities, return_json=True):
    """
    This function helps us to get locations info from data received from the AlchemyAPI

    Example received data from AlchemyAPI:
    {
     "keywords": ["intermingled careers", "Dnipropetrovsk Oblast"],
     "location": [[45.04, 35.67, 0.0]],
     "geo_entities": ["Dnipropetrovsk", "Lutsk"]
    }
    We operates here with location and geo_entities only as arrays

    :param array_coords: list()
    :param array_geo_entities: list()
    :param return_json: BOOLEAN
    :return: JSON or dict()
    """
    locations = {
        'coordinates': [],
        'geo_entity': None
    }
    index = 0
    for item in array_coords:
        lat = item[0]
        lng = item[1]
        point = dict()
        point['lat'] = lat
        point['lng'] = lng
        locations['coordinates'].append(point)
        locations['geo_entity'] = array_geo_entities[index]
        index += 1
    if return_json:
        return json.dumps(locations)
    else:
        return locations
