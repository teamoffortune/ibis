from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from ibis_core.articles.models import Article


class ArticleAdmin(SummernoteModelAdmin):
    list_display = ['article_url', 'title', 'authors', 'translated', 'post_date_crawled']
    list_filter = ('translated', 'access', 'status', 'channel')
    search_fields = ('search_query__id', 'search_query__main_query', 'article_url')


admin.site.register(Article, ArticleAdmin)
