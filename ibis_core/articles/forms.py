import json

from django import forms
from django_summernote.widgets import SummernoteWidget

from ibis_core.articles.models import Article


class ArticleEditForm(forms.ModelForm):
    tags = forms.CharField(required=False,
                           widget=forms.TextInput(attrs={
                               'class': 'form-control'}))

    class Meta:
        model = Article
        fields = ['body', 'translated_body', 'status', 'channel']
        widgets = {
            'body': SummernoteWidget(),
            'translated_body': SummernoteWidget(),
            'status': forms.Select(attrs={'class': 'form-control'}),
            'channel': forms.Select(attrs={'class': 'form-control'}),
        }

    def clean_tags(self):
        data = self.data.get('tags')
        cleaned_tags = list(set(data.split(',')))
        return cleaned_tags


class StatusChangeAjaxForm(forms.ModelForm):
    # status = forms.CharField(required=True, widget=forms.Select(attrs={
    #     'class': 'form-control'
    # }))
    class Meta:
        model = Article
        fields = ('status',)
        widgets = {
            'status': forms.Select(attrs={'class': 'form-control'}),
        }
