from __future__ import absolute_import, unicode_literals

import json
import logging

from autoslug.utils import slugify
from celery import shared_task
from django.db import IntegrityError
from django.db import transaction

from ibis_core.articles.management.commands.set_entities import parse_coordinates

from ibis_core.common.geo_entities_extractor import convert_to_json
from ibis_core.common.paragraps_processor import tag_p, split_by_sentences, untag
from ibis_core.tagging.models import Tag

log = logging.getLogger(__name__)


@shared_task(
    name='get_geo_entity_for_article',
    bind=True
)
def get_geo_entity_for_article(self, article_id):
    # Get article from DB
    from ibis_core.articles.models import Article
    try:
        article = Article.objects.get(id=article_id)
    except Article.DoesNotExist:
        self.retry(countdown=10, max_retries=5)
        article = None
    result = dict()
    if article:
        with transaction.atomic():
            try:
                try:
                    new_body = tag_p(split_by_sentences(untag(article.body)))
                except Exception:
                    new_body = None
                try:
                    new_translated_body = tag_p(split_by_sentences(untag(article.translated_body)))
                except IndexError:
                    new_translated_body = None

                if new_body and new_translated_body:
                    article.body = new_body
                    article.translated_body = new_translated_body
                # Call AlchemyAPI to provide an entities
                # They returned in JSON. So loads to the dict
                if new_translated_body:
                    entities = json.loads(convert_to_json(new_translated_body))
                    # Prepare locations data
                    article.locations = parse_coordinates(entities['location'], entities['geo_entities'])
                    # Get keyword tags and get_or_create Tag object by slug field.
                    tags = list()
                    # Check for tag in database or create one
                    if entities['keywords'] and len(entities['keywords']) > 0:
                        log.info("Keywords: %s" % entities['keywords'])
                        for tag_name in entities['keywords']:
                            obj, created = Tag.objects.get_or_create(
                                slug=slugify(tag_name),
                                defaults={'name': tag_name},
                            )
                            # Push created Tag object to the tags list
                            tags.append(obj)
                    # Add keyword
                    try:
                        article.tags.add(*tags)
                    except IntegrityError as e:
                        result['exception_msg'] = e
                    if not article.status:
                        article.set_article_status(article.search_query.source.split(', '))
                    article.save()
                    sid = transaction.savepoint()
                    transaction.savepoint_commit(sid)
                    log.info("Successfully SAVED %s article to DB" % article.id)
                    result['article_id'] = article.id
                    # result['tags_set'] = tags.__dict__
                    result['locations'] = article.locations
                else:
                    result['info'] = "Article's body din't translated." \
                                     "Can't detect entities by AlchemyAPI while article is not translated."
                    log.info(result['info'])

            except Exception as e:
                result['exception_msg'] = e
    else:
        result['error'] = 'Can\'t find article with id %s' % article_id
    return result
