import json

from django.http import HttpResponse
from django.http import JsonResponse
from django.utils.text import slugify
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView
from django.views.generic.edit import UpdateView, FormMixin, ProcessFormView
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response

from geopy.geocoders import Nominatim

from ibis_core.articles.forms import ArticleEditForm, StatusChangeAjaxForm
from ibis_core.articles.models import Article
from ibis_core.articles.serializers import ArticleSerializer
from ibis_core.common.mixins import UserSearchModeratorPermission, EditRightsPermision
from ibis_core.tagging.models import Tag


class AjaxableResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """
    def form_invalid(self, form):
        response = super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            # suggested_tags = self.object.tags.filter(slug__in=[])
            suggested_tags = ['mimi', 'BAR']
            data = {
                'suggested_tags': suggested_tags,
            }
            return JsonResponse(data)
        else:
            return response


class ArticlesList(generics.ListCreateAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    def create(self, request, *args, **kwargs):
        data = request.data
        articles = json.loads(data)

        serializer = self.get_serializer(data=articles, many=True)
        if serializer.is_valid():
            serializer.save()
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED,
                            headers=headers)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ArticleDetailView(LoginRequiredMixin, DetailView, FormMixin, ProcessFormView):
    model = Article
    form_class = StatusChangeAjaxForm

    def get_context_data(self, **kwargs):
        context = super(ArticleDetailView, self).get_context_data()
        # Forming map items list
        map_items = list()
        zindex_count = 0
        if self.object.locations:
            locations_str = ''.join(self.object.locations)
            locations = json.loads(locations_str)
            coord = list()
            for loc in locations['coordinates']:
                coord.append({
                    'lat': loc['lat'],
                    'lng': loc['lng'],
                    'zindex': zindex_count
                })
            map_items.append({
                'name': self.object.translated_title,
                'coord': coord,
            })
        context['map_items_json'] = json.dumps(map_items)
        return context

    def get_initial(self):
        return {'status': self.object.status}

    def post(self, request, *args, **kwargs):
        status = request.POST.get('status')
        data = dict()
        if status:
            obj = self.get_object()
            obj.status = status
            obj.save()
            data['result'] = 'Status changed to %s' % status
        return HttpResponse(json.dumps(data), 'application/json')


class ArticleUpdateView(LoginRequiredMixin, AjaxableResponseMixin, EditRightsPermision, UpdateView):
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = 'articles.edit_article'
    model = Article
    form_class = ArticleEditForm
    template_name_suffix = '_update_form'

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            results = []
            remove_tag = request.GET.get('remove_tag')
            if remove_tag is not None:
                data = dict()
                try:
                    # Get tag from db
                    tag = Tag.objects.get(slug=slugify(remove_tag))
                    # Remove m2m relation
                    self.object.tags.remove(tag)
                    data['result'] = 'success'
                    data['msg'] = 'Requested tag: [' + remove_tag + '] relation removed.'
                except Tag.DoesNotExist:
                    data['result'] = 'error'
                    data['msg'] = 'Requested tag: [' + remove_tag + '] does not exists.'

                return HttpResponse(json.dumps(data), 'application/json')

            add_location = request.GET.get('add_location')
            if add_location is not None:
                data = dict()
                geolocator = Nominatim()

                return HttpResponse(json.dumps(data), 'application/json')

            q = request.GET.get('term', '')
            suggested_tags = Tag.objects.filter(name__icontains=q)[:50]
            for tag in suggested_tags:
                tag_json = dict()
                tag_json['id'] = tag.id.urn[9:]
                tag_json['label'] = tag.name
                tag_json['value'] = tag.slug
                results.append(tag_json)
            data = json.dumps(results)
            return HttpResponse(data, 'application/json')
            # return JsonResponse(data, safe=False)
        else:
            return super(ArticleUpdateView, self).get(self.request)

    def get_initial(self):
        self.tag_names_list = [x.name for x in self.object.tags.all()]
        return {'tags': ", ".join(self.tag_names_list)}

    def get_context_data(self, **kwargs):
        context = super(ArticleUpdateView, self).get_context_data()

        context['tag_names'] = json.dumps(self.tag_names_list)

        # Forming map items list
        map_items = list()
        zindex_count = 0
        if self.object.locations:
            locations_str = ''.join(self.object.locations)
            locations = json.loads(locations_str)
            coord = list()
            for loc in locations['coordinates']:
                coord.append({
                    'lat': loc['lat'],
                    'lng': loc['lng'],
                    'zindex': zindex_count
                })
            map_items.append({
                'name': self.object.translated_title,
                'coord': coord,
            })
        context['map_items_json'] = json.dumps(map_items)

        return context

    def form_valid(self, form):
        form.save(commit=False)
        tags = form.cleaned_data['tags']
        if tags:
            for value in tags:
                try:
                    tag = Tag.objects.get(slug=slugify(value))
                    self.object.tags.add(tag)
                except Tag.DoesNotExist:
                    tag = Tag()
                    tag.name = value
                    tag.save()
                    self.object.tags.add(tag)
        return super(ArticleUpdateView, self).form_valid(form)
