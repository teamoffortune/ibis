from django.apps import AppConfig


class ArticlesConfig(AppConfig):
    name = 'ibis_core.articles'

    def ready(self):
        import ibis_core.articles.signals
