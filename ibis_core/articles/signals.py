from datetime import datetime, timedelta

from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils.timezone import utc

from ibis_core.articles.models import Article
from ibis_core.articles.tasks import get_geo_entity_for_article


@receiver(pre_save, sender=Article)
def set_important_fields(sender, instance, **kwargs):
    if instance.status == 'promoted' or instance.status == 'alert':
        # delta = datetime.utcnow().replace(tzinfo=utc) - instance.post_date_crawled.replace(tzinfo=utc)
        # if delta < timedelta(days=10):
            # Bind our article to the daily digest
            instance.send_to_digest()

# @receiver(post_save, sender=Article)
# def get_entities(sender, instance, created, **kwargs):
#     if created:
#         if not instance.locations or not instance.tags:
#             get_geo_entity_for_article.apply_async((instance.id,))
#             # get_geo_entity_for_article(instance.id)
