import json

from rest_framework import serializers

from autoslug.utils import slugify
from django.db import IntegrityError
from ibis_core.articles.models import Article
from ibis_core.searches.models import Search
from ibis_core.tagging.models import Tag


class TagsSerializer(serializers.Serializer):
    name = serializers.CharField(allow_blank=True, required=False)
    slug = serializers.CharField(allow_blank=True, required=False)

    # def to_representation(self, obj):
    #     options = json.loads(obj)
    #     return {
    #         'exactTerms': options['exactTerms'],
    #         'excludeTerms': options['excludeTerms']
    #     }

    # def to_internal_value(self, data):
    #     tags = json.dumps(data['tags'])
    #     tags_dict = dict()
    #     names = []
    #     for tag in tags:
    #         tags_dict['tags'] = names.append(tag['name'])
    #     return tags


class ArticleSerializer(serializers.ModelSerializer):
    article_url = serializers.CharField(required=False)
    search_id = serializers.CharField(required=False)
    top_image = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    top_image_url = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    tags = TagsSerializer(required=False, many=True)

    class Meta:
        model = Article

    def create(self, validated_data):
        try:
            article = Article.objects.get(article_url=validated_data['article_url'])
        except Article.DoesNotExist:
            article = Article(article_url=validated_data['article_url'])
        except Article.MultipleObjectsReturned:
            article = Article.objects.filter(article_url=validated_data['article_url']).first()

        article.source_language = validated_data['source_language']
        article.title = validated_data['title']
        article.translated_title = validated_data['translated_title']
        article.body = validated_data['body']
        article.translated_body = validated_data['translated_body']
        article.authors = validated_data['authors']
        if validated_data['post_date_created'] != '':
            article.post_date_created = validated_data['post_date_created']
        if validated_data['post_date_crawled'] != '':
            article.post_date_crawled = validated_data['post_date_crawled']
        article.translated = validated_data['translated']
        article.top_image_url = validated_data['top_image_url']
        if validated_data['top_image'] != '':
            article.top_image_path = validated_data['top_image']
        article.processed = validated_data['processed']
        related_search = self._get_related_search(validated_data['search_id'])
        article.channel = validated_data['channel']
        article.status = validated_data['status']
        article.locations = validated_data['locations']
        if related_search:
            article.search_query = related_search
            article.save()

        # Check for arrived tags
        tags = list()
        tag_objects = validated_data['tags']
        for tag in tag_objects:
            tags.append(tag['name'])
        internal_tags = Tag.objects.filter(name__in=tags)
        # if len(tag_objects) > 0:
        #     for tag_object in tag_objects:
        #         # Check for tag in database or create one
        #         obj, created = Tag.objects.get_or_create(
        #             slug=slugify(tag_object['name']),
        #             defaults={'name': tag_object['name']},
        #         )
        #         # Push created Tag object to the tags list
        #         tags.append(obj)
        # Add keyword
        try:
            article.tags.add(*internal_tags)
        except IntegrityError:
            pass
        except ValueError:
            pass

    def _get_related_search(self, search_id):
        try:
            return Search.objects.get(pk=search_id)
        except Search.DoesNotExist:
            return None
