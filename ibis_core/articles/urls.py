from django.conf.urls import url, include
from rest_framework import routers
from ibis_core.articles.views import ArticlesList, ArticleDetailView, ArticleUpdateView


class SearchTypesList(object):
    pass


urlpatterns = [
    # URL pattern for the ArticleDetailView
    url(
        regex=r'^(?P<pk>[\w-]+)$',
        view=ArticleDetailView.as_view(),
        name='detail'
    ),
    # URL pattern for the ArticleUpdateView
    url(
        regex=r'^(?P<pk>[\w-]+)/~update$',
        view=ArticleUpdateView.as_view(),
        name='update'
    ),
    url(r'', ArticlesList.as_view()),

    url(r'^', include('rest_framework.urls', namespace='rest_framework')),


]
