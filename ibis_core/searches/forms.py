import ast
import datetime
import json

from django import forms
from django.contrib.postgres.search import SearchVector, SearchQuery
from django.forms import ModelForm, TextInput, Select, model_to_dict, \
    BaseModelForm, RadioSelect, Form, CheckboxSelectMultiple, DateInput, ChoiceField, SelectMultiple
from django.forms.utils import ErrorList

from ibis_core.articles.models import Article
from ibis_core.common.utils import get_searches_list_for_themes, do_filtered_search
from ibis_core.issues.models import Issue
from ibis_core.searches.models import Search, SuggestedSearch
from ibis_core.themes.models import Theme


class SearchForm(ModelForm):
    test_search = forms.CharField(
        required=False,
        widget=TextInput()
    )
    all_words = forms.CharField(
            label='All of these words',
            required=False,
            widget=TextInput(attrs={'class': 'form-control'})
        )
    exact_phrase = forms.CharField(
        label='This exact phrase or word',
        required=False,
        widget=TextInput(attrs={'class': 'form-control'})
    )

    omit_words = forms.CharField(
        label='Omit these words',
        required=False,
        widget=TextInput(attrs={'class': 'form-control'})
    )

    source = forms.CharField(
        label='Source',
        required=False,
        widget=SelectMultiple(attrs={'class': 'form-control', 'id': 'source-select', 'multiple': 'multiple'})
    )

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix=None,
                 empty_permitted=False, instance=None):
        opts = self._meta
        if opts.model is None:
            raise ValueError('ModelForm has no model class specified.')
        if instance is None:
            # if we didn't get an instance, instantiate a new one
            self.instance = opts.model()
            object_data = {}
        else:
            self.instance = instance
            object_data = model_to_dict(instance, opts.fields, opts.exclude)
            # Extract query params
            if instance.options:
                object_data.update(instance.options['query_params'])
        # if initial was provided, it should override the values from instance
        if initial is not None:
            object_data.update(initial)

        # self._validate_unique will be set to True by BaseModelForm.clean().
        # It is False by default so overriding self.clean() and failing to call
        # super will stop validate_unique from being called.
        self._validate_unique = False
        super(BaseModelForm, self).__init__(data, files, auto_id, prefix, object_data,
                                            error_class, label_suffix, empty_permitted)

        # Apply ``limit_choices_to`` to each field.
        for field_name in self.fields:
            formfield = self.fields[field_name]
            if hasattr(formfield, 'queryset') and hasattr(formfield, 'get_limit_choices_to'):
                limit_choices_to = formfield.get_limit_choices_to()
                if limit_choices_to is not None:
                    formfield.queryset = formfield.queryset.complex_filter(limit_choices_to)

    def clean(self):
        cleaned_data = super(SearchForm, self).clean()
        test_search = cleaned_data.get('test_search')
        name = cleaned_data.get('name')
        type = cleaned_data.get('type')
        main_query = cleaned_data.get('main_query')
        rss_link = cleaned_data.get('rss_link')
        direct_url = cleaned_data.get('direct_url')
        period = cleaned_data.get('period')
        channel = cleaned_data.get('channel')

        # We should parse source parameter because it arrives as string repr of list
        # and redefine the cleaned_data['source']
        source = self.data.getlist('source')
        source_str = ', '.join(x for x in source)
        self.cleaned_data['source'] = source_str

        all_words = cleaned_data.get('all_words')
        exact_phrase = cleaned_data.get('exact_phrase')
        omit_words = cleaned_data.get('omit_words')

        if test_search:
            if main_query == '':
                msg = "You must provide a main query."
                self.add_error('main_query', msg)
            self.add_error('test_search', '')

        if type in ['simple_search', 'search_engine']:
            if not main_query:
                msg = "You must provide a main query."
                self.add_error('main_query', msg)
            if not source:
                msg = "You must provide source value."
                self.add_error('source', msg)

        elif type == 'rss' and not rss_link:
            msg = "You must provide an RSS link."
            self.add_error('rss_link', msg)

        elif type == 'article' and not direct_url:
            msg = "You must provide a direct link."
            self.add_error('direct_url', msg)

        if name is None or len(name) == 0:
            self.cleaned_data['name'] = main_query

        return self.cleaned_data

    class Meta:
        model = Search
        fields = ['name', 'type', 'main_query', 'rss_link', 'direct_url', 'period', 'source', 'channel']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control'}),
            'type': RadioSelect(attrs={}),
            'main_query': TextInput(attrs={'class': 'form-control'}),
            'rss_link': TextInput(attrs={'class': 'form-control'}),
            'direct_url': TextInput(attrs={'class': 'form-control'}),
            'period': Select(attrs={'class': 'form-control'}),
            'source': CheckboxSelectMultiple(attrs={'class': 'form-control'}),
            'channel': Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'main_query': 'Search'
        }


class EditForm(SearchForm):
    all_words = forms.CharField(
        label='All of these words',
        required=False

    )
    omit_words = forms.CharField(
        label='Omit these words',
        required=False
    )


ARTICLE_STATUS_CHOICES = Article.STATUSES
ARTICLE_CHANNELS_CHOICES = Article.CHANNELS


class SearchFilterForm(Form):

    def __init__(self, theme_choices, last_days_choices, paginate_choices, *args, **kwargs):
        super(SearchFilterForm, self).__init__(*args, **kwargs)
        # Get user themes choices at form instantiating to display in search form
        self.fields['theme'].choices = theme_choices
        # Provide choices for last_days selector
        self.fields['last_days'].choices = last_days_choices
        self.fields['paginate_by'].choices = paginate_choices

    q = forms.CharField(
        required=False,
        widget=TextInput(attrs={'class': 'form-control', 'placeholder': 'Search', 'form': 'search-list-filter'})
    )

    article_status = forms.MultipleChoiceField(
        label='Article Status',
        required=False,
        choices=ARTICLE_STATUS_CHOICES,
        widget=CheckboxSelectMultiple(attrs={'type': 'checkbox', 'form': 'search-list-filter'}),
    )
    theme = forms.MultipleChoiceField(
        label='Theme',
        required=False,
        widget=CheckboxSelectMultiple(attrs={'type': 'checkbox', 'form': 'search-list-filter'}),
    )
    channel = forms.MultipleChoiceField(
        label='Channels',
        required=False,
        choices=ARTICLE_CHANNELS_CHOICES,
        widget=CheckboxSelectMultiple(attrs={'type': 'checkbox', 'form': 'search-list-filter'}),
    )
    last_days = ChoiceField(
        label='Last Days',
        required=False,
        widget=Select(attrs={'class': 'form-control', 'form': 'search-list-filter'})
    )
    date_start = forms.DateField(
        label='Date From',
        required=False,
        widget=DateInput(attrs={'type': 'date', 'class': 'form-control', 'form': 'search-list-filter'})
    )
    date_stop = forms.DateField(
        label='Date To',
        required=False,
        widget=DateInput(attrs={'type': 'date', 'class': 'form-control', 'form': 'search-list-filter'})
    )
    paginate_by = forms.ChoiceField(
        required=False,
        widget=Select(attrs={'class': 'form-control', 'form': 'articles-table'}),
        initial=25
    )

    def filter_queryset(self):
        if hasattr(self, 'cleaned_data'):
            return do_filtered_search(**self.cleaned_data)
        else:
            return do_filtered_search(**dict())


class SaveSearchForm(ModelForm):
    class Meta:
        model = SuggestedSearch
        fields = ['name', 'query', 'initials', 'user']


class ActivateForm(ModelForm):
    class Meta:
        model = Search
        fields = []
