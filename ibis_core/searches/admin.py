from django.contrib import admin

# Register your models here.
from ibis_core.searches.models import Search, SuggestedSearch

admin.site.register(Search)
admin.site.register(SuggestedSearch)
