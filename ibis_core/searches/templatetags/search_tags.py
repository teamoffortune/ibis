from django import template

register = template.Library()


@register.inclusion_tag('searches/templatetags/search_info.html')
def search_info(search):
    search_issues = []
    search_groups = []
    search_themes = []
    for issue in search.issues.all():
        search_issues.append(issue)
        for group in issue.groups.all():
            search_groups.append(group.group)
            for theme in group.group.theme.all():
                search_themes.append(theme)

    return {'issues': set(search_issues), 'groups': set(search_groups), 'themes': set(search_themes)}
