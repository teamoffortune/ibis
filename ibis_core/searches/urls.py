# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    # URL pattern for the SearchListView
    url(
        regex=r'^$',
        view=views.SearchListView.as_view(),
        name='list'
    ),

    # URL pattern for the CreateIssue
    url(
        regex=r'^~new/$',
        view=views.SearchCreateView.as_view(),
        name='create'
    ),
    # URL pattern for the SearchDetailView
    url(
        regex=r'^(?P<pk>[\w-]+)$',
        view=views.SearchDetailView.as_view(),
        name='detail'
    ),
    # URL pattern for the SearchUpdateView
    url(
        regex=r'^(?P<pk>[\w-]+)/~update$',
        view=views.SearchUpdateView.as_view(),
        name='update'
    ),

    url(
        regex=r'^(?P<pk>[\w-]+)/~activestate$',
        view=views.SearchChangeActiveStateView.as_view(),
        name='activestate'
    ),

    url(
        regex=r'^filters/',
        view=views.SearchFilteringView.as_view(),
        name='filters'
    ),

    # url(
    #     regex=r'^~save-search/$',
    #     view=views.SaveSearchFiltersView.as_view(),
    #     name='save_search'
    # ),




    # url(
    #     regex=r'^(?P<pk>[\w-]+)/~deactivate$',
    #     view=views.SearchDeactivateView.as_view(),
    #     name='deactivate'
    # ),
]
