import hashlib
import json
import uuid

from django.contrib import messages
from django.contrib.postgres.search import SearchVector
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.views.generic import TemplateView
from django.views.generic.detail import SingleObjectMixin, SingleObjectTemplateResponseMixin
from googleapiclient.discovery import build

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.core.cache import caches
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render_to_response
from django.views.generic import ListView, DetailView
from django.views.generic import View
from django.views.generic.edit import CreateView, UpdateView, FormMixin, FormView, ProcessFormView, BaseCreateView, \
    ModelFormMixin
from math import ceil

from ibis_core.articles.models import Article
from ibis_core.common.constants import PUBLIC
from ibis_core.common.mixins import UserIssueSearchModeratorPermission, UserSearchModeratorPermission
from ibis_core.common.utils import get_searches_list_for_user, do_filtered_search, get_issues_list_for_user
from ibis_core.issues.forms import CreateIssueForm
from ibis_core.issues.models import Issue
from ibis_core.searches.forms import SearchForm, ActivateForm, SearchFilterForm, SaveSearchForm
from ibis_core.searches.models import Search, SuggestedSearch
from ibis_core.themes.models import Theme


LAST_DAYS_PERIODS_CHOICES = (
    (None, 'All'),
    (1, 'Last 1 Day'),
    (3, 'Last 3 Days'),
    (5, 'Last 5 Days'),
    (7, 'Last 7 Days'),
    (15, 'Last 15 Days'),
    (30, 'Last 30 Days'),
    (60, 'Last 60 Days'),
    (90, 'Last 90 Days')
)

PAGINATE_CHOICES = (
    (10, '10'),
    (25, '25'),
    (50, '50'),
    (100, '100')
)


def get_user_allowed_issues(user):
    """
    Retuns issues list where user is moderator or follower
    :param user:
    :return: issues queryset
    """
    return Issue.objects.filter(members__user_id=user.id)


def cse_call(request, *args, **kwargs):
    params_map = dict(
        q='main_query',
        exactTerms='exact_phrase',
        excludeTerms='omit_words'
    )

    make_request = False

    request_data = {}

    for query_key, get_name in params_map.items():
        q_value = request.POST.get(get_name, None)
        if q_value is not None and len(q_value) > 0:
            make_request = True
            request_data.update({query_key: q_value})

    if make_request:
        h = hashlib.new('ripemd160')
        h.update(json.dumps(request_data).encode())
        cache_key = h.hexdigest()
        cache = caches['default']

        res = cache.get(cache_key)
        if res is None:
            res = []

            request_data.update(dict(cx='012065428559517091395:-hbqhgchz-y'))
            service = build('customsearch', 'v1', developerKey='AIzaSyDhXzmHAl2SBA8UZzfJlABO79-ZairSPu0')
            for page_num in range(9):
                request_data.update({'start': 10*page_num+1})
                g_res = service.cse().list(
                    **request_data
                ).execute()
                if 'items' in g_res:
                    res.extend(g_res['items'])

            cache.set(cache_key, res)

        return res


class SearchCreateView(LoginRequiredMixin, UserIssueSearchModeratorPermission,
                       CreateView):
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = 'searches.add_search'
    model = Search
    form_class = SearchForm
    initial = {
        'type': 'simple_search'  # Set a default search
    }

    def form_valid(self, form):
        # We should add the user as issue author and moderator
        form.instance.author = self.request.user
        self.object = form.save()
        # Save search to selected issues if we just create new search (not from issues form)

        query_params_fields = ['all_words', 'exact_phrase', 'omit_words']

        query_params = {}
        for field_name in query_params_fields:
            query_params[field_name] = form.cleaned_data[field_name]

        selected_issues = self.request.POST.getlist('issue')
        if self.slug_url_kwarg in self.kwargs:
            # Here we create search for issue
            # if slug in url we suppose that search created from issue form
            issue_instance = get_object_or_404(
                Issue,
                slug=self.kwargs.get(self.slug_url_kwarg)
            )
            selected_issues = set(selected_issues).union(set([issue_instance.id]))

        if len(selected_issues):
            [self.object.issues.add(issue_instance) for issue_instance in Issue.objects.filter(id__in=selected_issues)]

        #TODO: This was not clarified so try to get first one
        try:
            if len(selected_issues) and self.object.issues.all()[0].access == PUBLIC:
                self.object.create_event(actor=self.request.user)
        except Exception as e:
            pass

        options_dict = {
            'query_params': query_params
        }
        form.instance.options = options_dict

        return super(SearchCreateView, self).form_valid(form)

    def form_invalid(self, form):
        context = self.get_context_data(form=form)
        test_search = self.request.POST.get('test_search', None)
        main_query = self.request.POST.get('main_query', None)
        context['selected_issues'] = [uuid.UUID(id) for id in self.request.POST.getlist('issue')]
        if test_search and main_query != '':
            # Add in the results
            context['searches'] = cse_call(self.request)

        return self.render_to_response(context)

    def get_success_url(self):
        # Sends the user back to the searches list or to the issue detail page
        if self.slug_url_kwarg in self.kwargs:
            issue_instance = get_object_or_404(
                Issue,
                slug=self.kwargs.get(self.slug_url_kwarg)
            )
            return reverse(
                'issues:detail',
                kwargs={'slug': issue_instance.slug}
            )
        else:
            return reverse('searches:list')

    def get_context_data(self, **kwargs):
        context = super(SearchCreateView, self).get_context_data()
        context['issues'] = get_user_allowed_issues(self.request.user)
        return context


class SearchListView(LoginRequiredMixin, ListView):
    model = Search

    def get_queryset(self):
        # We should return searches where user is author or follow or issue
        # searches
        if self.request.user.is_superuser:
            qs = super(SearchListView, self).get_queryset().order_by('-created')
        else:
            qs = get_searches_list_for_user(self.request.user)

        if 'q' in self.request.GET:
            q_param = self.request.GET.get('q')
            qs = qs.filter(name__icontains=q_param).order_by('-created')

        return qs


class SearchDetailView(LoginRequiredMixin, DetailView):
    model = Search
    page_size = 10

    def get_context_data(self, **kwargs):
        """
        Enrich context with crawled articles
        """
        context = super(SearchDetailView, self).get_context_data(**kwargs)

        articles_qs = self.object.articles.order_by('-created')
        paginator = Paginator(articles_qs, self.page_size)
        page = self.request.GET.get('page')

        try:
            article_list = paginator.page(page)
        except PageNotAnInteger:
            article_list = paginator.page(1)
        except EmptyPage:
            article_list = paginator.page(paginator.num_pages)

        context['articles'] = article_list

        # Check whether user has edit rights
        context['can_edit'] = context['object'].has_edit_rights(self.request.user)
        context.update(kwargs)
        return context


class SearchUpdateView(LoginRequiredMixin, UserSearchModeratorPermission, UpdateView):
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = 'searches.change_search'
    model = Search
    form_class = SearchForm
    # fields = ['name']
    template_name_suffix = '_form'

    def get_initial(self):
        initial = super(SearchUpdateView, self).get_initial()
        try:
            search_type = self.object.type
        except:
            search_type = 'search_engine'
        # else:
        initial['type'] = search_type
        initial['source'] = self.object.source.split(', ')

        return initial

    def form_valid(self, form):
        self.object = form.save()

        query_params_fields = ['all_words', 'exact_phrase', 'omit_words']

        query_params = {}
        for field_name in query_params_fields:
            query_params[field_name] = form.cleaned_data[field_name]

        # Save search to selected issues if we just create new search (not from issues form)
        selected_issues = self.request.POST.getlist('issue')
        existed_issues = [str(id) for id in self.object.issues.all().values_list('id', flat=True)]

        issues_to_add = set(selected_issues) - set(existed_issues)
        issues_to_del = set(existed_issues) - set(selected_issues)

        if len(issues_to_del):
            [self.object.issues.remove(issue) for issue in Issue.objects.filter(id__in=issues_to_del)]

        if len(issues_to_add):
            [self.object.issues.add(issue_instance) for issue_instance in Issue.objects.filter(id__in=issues_to_add)]

        options_dict = {
            'query_params': query_params
        }
        form.instance.options = options_dict
        return super(SearchUpdateView, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        # As we have a UserSearchModeratorPermission mixin here we should have object already (prevents double query)
        if self.object is None:
            self.object = self.get_object()

        return self.render_to_response(self.get_context_data())

    def get_success_url(self):
        return reverse('searches:list')

    def get_context_data(self, **kwargs):
        context = super(SearchUpdateView, self).get_context_data(**kwargs)
        context['edit_mode'] = True
        context['issues'] = get_user_allowed_issues(self.request.user)
        context['selected_issues'] = context['object'].issues.all().values_list('id', flat=True)
        return context


class SearchChangeActiveStateView(LoginRequiredMixin, UserSearchModeratorPermission, UpdateView):
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = 'searches.change_search'
    model = Search
    form_class = ActivateForm

    def form_valid(self, form):
        form.instance.active = bool(int(self.request.POST.get('active')))
        return super(SearchChangeActiveStateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('searches:list')


class SearchFilteringView(FormMixin, TemplateView):
    form_class = SearchFilterForm
    template_name = 'searches/filtered_search.html'
    context_object_name = 'articles'
    paginate_by = 25

    def get_form_kwargs(self):
        return {
            'initial': self.get_initial(),
            'prefix': self.get_prefix(),
            'data': self.request.GET or None,
            # Provide a request.user's themes list for setting themes choices in filter
            'theme_choices': self._get_theme_choices(Theme.objects.filter(members__user=self.request.user).all()),
            'last_days_choices': LAST_DAYS_PERIODS_CHOICES,
            'paginate_choices': PAGINATE_CHOICES
        }

    def get_context_data(self, **kwargs):
        context = super(SearchFilteringView, self).get_context_data(**kwargs)

        paginate_by = self.request.GET.get('paginate_by', self.paginate_by)
        page = self.request.GET.get('page', 1)

        try:
            page = int(page)
        except ValueError:
            page = 1

        try:
            paginate_by = int(paginate_by)
        except ValueError:
            paginate_by = self.paginate_by

        try:
            all_user_themes = kwargs['form'].fields['theme'].choices
        except IndexError or ValueError:
            all_user_themes = []

        article_list, total = do_filtered_search(
            request=self.request,
            page=page,
            paginate_by=paginate_by
        )

        num_pages = int(ceil(total / float(paginate_by)))

        page_obj = dict(
            number=page,
            has_previous=page > 1,
            has_next=page < total,
            previous_page_number=max(1, page-1),
            next_page_number=min(num_pages, page + 1)
        )

        context['articles'] = article_list
        context['user_issues'] = get_issues_list_for_user(self.request.user)
        context['paginator'] = dict(num_pages=num_pages)
        context['num_pages'] = num_pages
        context['page_obj'] = page_obj
        context['is_paginated'] = page_obj['has_previous'] or page_obj['has_next']

        return context

    def get(self, request, *args, **kwargs):
        self.suggested_searches = SuggestedSearch.objects.filter(user=request.user)

        form = self.get_form(self.get_form_class())
        save_form = SaveSearchForm()
        issue_form = CreateIssueForm()

        if save_form.is_valid():
            save_form.save()

        context = self.get_context_data(
            form=form,
            save_form=save_form,
            issue_form=issue_form,
            suggested_searches=self.suggested_searches
        )

        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        form = SaveSearchForm(request.POST)
        if form.is_valid():
            search_name = form.cleaned_data['name']
            user = form.cleaned_data['user']
            # Check if user already have suggestedz search with this name
            try:
                search = SuggestedSearch.objects.get(name=search_name, user=user)
                messages.add_message(
                    request,
                    messages.INFO,
                    'You already have a search with this parameters',
                    fail_silently=True,
                )
            except SuggestedSearch.DoesNotExist:
                form.save()
        return self.get(request)

    def _get_theme_choices(self, themes_list):
        choices = []
        for theme in themes_list:
            choices.append((theme.slug, theme.name))
        return choices


class TestGoogleCSEQuery(View):

    def get(self, request, *args, **kwargs):
        params_map = dict(
            q='q',
            exactTerms='exact_phrase',
            excludeTerms='omit_words'
        )

        make_request = False

        request_data = {}

        for query_key, get_name in params_map.items():
            q_value = request.GET.get(get_name, None)
            if q_value is not None and len(q_value) > 0:
                make_request = True
                request_data.update({query_key: q_value})

        if make_request:
            h = hashlib.new('ripemd160')
            h.update(json.dumps(request_data).encode())
            cache_key = h.hexdigest()
            cache = caches['default']

            res = cache.get(cache_key)
            if res is None:
                request_data.update(dict(cx='012065428559517091395:-hbqhgchz-y'))
                service = build('customsearch', 'v1', developerKey='AIzaSyDhXzmHAl2SBA8UZzfJlABO79-ZairSPu0')
                res = service.cse().list(
                    **request_data
                ).execute()

                cache.set(cache_key, res)

            return JsonResponse(res)

        return JsonResponse([])
