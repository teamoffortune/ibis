# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-21 14:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('searches', '0012_auto_20160919_1451'),
    ]

    operations = [
        migrations.AlterField(
            model_name='search',
            name='name',
            field=models.CharField(max_length=200),
        ),
    ]
