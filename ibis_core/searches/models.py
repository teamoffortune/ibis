import hashlib

import requests
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save
from django.dispatch import receiver

from ibis_core.activity.models import EventMixin
from ibis_core.common.crawler_client import CrawlerClient
from ibis_core.common.models import AbstractBaseModel, AbstractNamedModel
from ibis_core.issues.models import Issue, IssueUsers
from ibis_core.users.models import User
from ibis_core.common.constants import CHANNELS


def get_sources_list():
    try:
        sources_list = CrawlerClient().get_sources_list()
    except:
        sources_list = [
            ['google', 'Google'],
            ['google_cse', 'Google CSE'],
            ['google_blogs', 'Google Blogs'],
            ['google_news', 'Google News'],
            ['google_scholar', 'Google Scholar'],
            # ['bing', 'Bing'],
            # ['yandex', 'Yandex']
        ]

    return sources_list


def get_search_type_list():
    try:
        type_list = CrawlerClient().get_search_types_list()
    except:
        type_list = [
            ['simple_search', 'Simple Search'],
            ['search_engine', 'Advanced Search'],
            ['rss', 'RSS Feed'],
            ['article', 'Article'],
            ['email', 'Email']
        ]

    return type_list


def get_search_query_hash(object):
    to_hash = ''
    h = hashlib.new('ripemd160')
    if object.type == 'search_engine':
        to_hash += object.main_query
    elif object.type == 'rss':
        to_hash += object.rss_link
    elif object.type == 'article':
        to_hash += object.direct_url
    h.update(to_hash.encode())
    return h.hexdigest()


class Search(AbstractBaseModel, EventMixin):
    """
    Represents search instance. Search doesn't need to be associated with an
    Issue, so foreign key can be empty.
    """

    PERIODS = (
        ('hourly', 'Hourly'),
        ('daily', 'Daily'),
        ('weekly', 'Weekly'),
        ('monthly', 'Monthly')
    )

    class Meta:
        ordering = ['-created']

    issues = models.ManyToManyField(Issue, related_name='searches', blank=True, db_index=True)

    author = models.ForeignKey(User, related_name='searches')

    # Query for searching at search engines - if provided
    main_query = models.TextField(null=True, blank=True)

    # Direct RSS link - if provided
    rss_link = models.CharField(max_length=1000, blank=True, null=True)

    # Direct News/Blog article url - if provided
    direct_url = models.CharField(max_length=1000, blank=True, null=True)

    # Should help to compose bunch of subqueries
    query_template = models.TextField(null=True, blank=True)

    # Stores user generated or automatically generated similar queries
    subqueries = JSONField(blank=True, null=True)

    # If false the system shouldn't serve the search
    active = models.BooleanField(default=True)

    options = JSONField(blank=True, null=True)

    period = models.CharField(max_length=20, choices=PERIODS, default='daily', blank=True)

    source = models.CharField(max_length=200, default='google_cse',
                              help_text='Type please with 1 backspace and 1 coma btw words. Ex.: google, yahoo')

    channel = models.CharField(max_length=64, blank=True, choices=CHANNELS, default='general')

    type = models.CharField(max_length=50, choices=get_search_type_list(), default='')

    name = models.CharField(max_length=1000, unique=True, blank=True)

    def get_update_url(self):
        return reverse('searches:update', args=[str(self.id)])

    def has_edit_rights(self, user):
        """
        Checks whether user has rights to edit the search
        :param user: user
        :return: BOOL
        """
        result = user == self.author
        for issue in self.issues.all():
            result = result or user in issue.member_list()

        return result or user.is_ibis_superadmin

    def __str__(self):
        return '{0} - {1} / [type: {2}]'.format(self.name if self.name else 'Unnamed search', self.period,
                                                self.type)

    @models.permalink
    def get_absolute_url(self):
        return 'searches:detail', (), {'pk': self.id}


class SuggestedSearch(AbstractNamedModel):
    """
    Suggested searches based on user filters and search queries that user saves on searches:filters page
    """
    query = models.TextField(null=True, blank=True)
    initials = models.TextField(blank=True, null=True)
    user = models.ForeignKey('users.User')


@receiver(post_save, sender=Search)
def my_handler(sender, instance, created, **kwargs):

    data = dict(
        search_id=instance.id,
        search_type=instance.type,
        rss_link=instance.rss_link,
        article_url=instance.direct_url,
        query=instance.main_query,
        source=instance.source,
        active=instance.active,
        period=instance.period,
        channel=instance.channel
    )

    try:
        CrawlerClient().create_or_update_search_query(data=data)
    except requests.exceptions.ConnectionError:
        pass
