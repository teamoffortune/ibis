from django.apps import AppConfig


class SearchesConfig(AppConfig):
    name = 'ibis_core.searches'
