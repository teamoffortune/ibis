from django.apps import AppConfig


class DashboardConfig(AppConfig):
    name = 'ibis_core.dashboard'
