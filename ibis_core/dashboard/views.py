from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView

from ibis_core.activity.models import Event
from ibis_core.articles.models import Article
from ibis_core.common.utils import get_groups_list_for_user, get_issues_list_for_user, get_searches_list_for_user, \
    get_articles_list_for_user, do_filtered_search
from ibis_core.groups.models import Group
from ibis_core.issues.models import Issue
from ibis_core.searches.models import Search


class DashboardView(TemplateView):
    """
    View for serving dashboard
    """
    template_name = 'dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        user = self.request.user
        if user.is_authenticated():
            user_groups = get_groups_list_for_user(user)
            user_issues = get_issues_list_for_user(user)
            user_searches = get_searches_list_for_user(self.request.user)
            user_articles, total = do_filtered_search(request=self.request, return_total=False)
            user_activities = Event.objects.get_user_activity_stream(user)[:12]

            context['user_groups'] = user_groups
            context['user_issues'] = user_issues
            context['user_searches'] = user_searches
            context['user_articles'] = user_articles
            context['user_activities'] = user_activities

        return context
