from django.contrib import admin

from ibis_core.tagging.models import Tag


class TagAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'id']
    search_fields = ['id']


admin.site.register(Tag, TagAdmin)
