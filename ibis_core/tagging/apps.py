from django.apps import AppConfig


class TaggingConfig(AppConfig):
    name = 'ibis_core.tagging'
