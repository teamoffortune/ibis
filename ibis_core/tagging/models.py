from autoslug import AutoSlugField
from ibis_core.common.model_mixins import ModelAccessMixin
from ibis_core.common.models import AbstractNamedModel
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Tag(MPTTModel, AbstractNamedModel, ModelAccessMixin):
    """
    Class for tagging/classification instances
    """
    slug = AutoSlugField(populate_from='name', always_update=True, unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

    class MPTTMeta:
        order_insertion_by = ['name']
