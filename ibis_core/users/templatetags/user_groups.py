from django import template
from django.contrib.auth.models import Group

from ibis_core.themes.models import Theme

register = template.Library()


@register.filter(name='has_group')
def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return True if group in user.groups.all() else False


@register.filter(name='is_theme_member')
def is_theme_member(user, theme_slug):
    theme = Theme.objects.get(slug=theme_slug)
    return True if user in theme.members else False
