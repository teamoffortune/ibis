from django import forms
from django.contrib.auth.models import Group as AuthGroup

from ibis_core.users.models import User


class UserCreateForm(forms.ModelForm):
    groups = forms.ModelMultipleChoiceField(
        queryset=AuthGroup.objects.all(),
        label='Permission Group'
    )

    email = forms.CharField(required=True)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password', 'groups', 'is_subscribed']
        widgets = {
            'first_name': forms.TextInput(attrs={'style': "text-transform: capitalize;"}),
            'last_name': forms.TextInput(attrs={'style': "text-transform: capitalize;"}),
            'email': forms.TextInput(attrs={'style': "text-transform: lowercase;"}),
            'password': forms.PasswordInput()
        }

    def save(self, commit=True):
        self.user = super(UserCreateForm, self).save()

        password = self.cleaned_data["password"]
        self.user.set_password(password)
        if commit:
            self.user.save()
        return self.user


class UserEditForm(forms.ModelForm):
    groups = forms.ModelMultipleChoiceField(
        queryset=AuthGroup.objects.all(),
        label='Permission Group'
    )

    def __init__(self, user, *args,**kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)
        if not user.is_ibis_superadmin:
            del self.fields['groups']

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'groups', 'is_subscribed']
        widgets = {
            'first_name': forms.TextInput(attrs={'style': "text-transform: capitalize;"}),
            'last_name': forms.TextInput(attrs={'style': "text-transform: capitalize;"}),
            'email': forms.TextInput(attrs={'style': "text-transform: lowercase;"}),
        }
