# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.contrib.auth.models import Group
from allauth.account.adapter import get_adapter
from allauth.account.signals import email_confirmed
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class User(AbstractUser):
    is_subscribed = models.BooleanField(default=False)

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})

    def clean(self):
        self.username = self.username.lower()

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)

    @property
    def is_ibis_superadmin(self):
        try:
            group = Group.objects.get(name='Superadmin')
            return group in self.groups.all()
        except Group.DoesNotExist:
            return False

    @property
    def is_ibis_admin(self):
        try:
            group = Group.objects.get(name='Admin')
            return group in self.groups.all()
        except Group.DoesNotExist:
            return False


def email_confirmed_callback(**kwargs):
    request = kwargs['request']
    email_addr = kwargs['email_address']
    try:
        user = User.objects.get(email=email_addr.email)
    except User.DoesNotExist:
        return

    ctx = dict(
        user=user,
        email=email_addr.email,
        current_site=get_current_site(request)
    )
    email_template = 'account/email/email_confirmed'
    # subject = render_to_string('account/email/email_confirmed_subject.txt')
    # subject = " ".join(subject.splitlines()).strip()
    # body = render_to_string('account/email/email_confirmed_message.txt', ctx)

    adapter = get_adapter(request)
    adapter.send_mail(email_template,
                       email_addr.email,
                       ctx)

email_confirmed.connect(email_confirmed_callback)

