# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import uuid

from allauth.account.utils import setup_user_email, send_email_confirmation
from django.conf import settings
from django.core.exceptions import PermissionDenied

from django.core.urlresolvers import reverse
from django.db import transaction
from django.views.generic import CreateView
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from django.contrib.auth.models import Group as AuthGroup

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic.edit import ModelFormMixin

from ibis_core.common.utils import get_groups_list_for_user, get_issues_list_for_user, get_searches_list_for_user
from ibis_core.groups.models import Group, GroupUsers
from ibis_core.issues.models import Issue, IssueUsers
from ibis_core.themes.models import Theme, ThemeUsers
from ibis_core.users.forms import UserEditForm, UserCreateForm
from .models import User


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'
    template_name = 'users/user_profile.html'

    def get_context_data(self, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)

        context['user_themes'] = Theme.objects.filter(members__user=self.request.user).all()
        context['user_groups'] = get_groups_list_for_user(self.request.user)
        context['user_issues'] = get_issues_list_for_user(self.request.user)
        context['user_searches'] = get_searches_list_for_user(self.request.user)

        return context


class UserProfileView(UserDetailView):

    def get_object(self, queryset=None):
        return self.request.user


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse('dashboard')


class UserListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = ('users.add_user', 'users.change_user')
    raise_exception = True
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'
    selected_filters = {}
    themes_list = []
    groups_list = []
    issues_list = []

    def get_queryset(self):
        filters = {
            'themes': 'theme-filter',
            'groups': 'group-filter',
            'issues': 'issue-filter'
        }
        self.selected_filters = {}
        queryset = super(UserListView, self).get_queryset()

        if self.request.user.is_ibis_superadmin:
            self.themes_list = Theme.objects.all()
            self.groups_list = Group.objects.all()
            self.issues_list = Issue.objects.all()
        else:
            self.themes_list = Theme.objects.filter(
                id__in=self.request.user.themes.filter(is_moderator=True).values_list('theme_id', flat=True)
            )
            self.groups_list = get_groups_list_for_user(self.request.user)
            self.issues_list = get_issues_list_for_user(self.request.user)

        theme_filter_values = self.request.GET.getlist('theme-filter')

        if len(theme_filter_values):
            if 'all' in theme_filter_values:
                self.selected_filters['themes'] = 'all'
            else:
                self.selected_filters['themes'] = [uuid.UUID(theme_id) for theme_id in theme_filter_values]
                queryset = queryset.filter(themes__theme__in=theme_filter_values)
        else:
            if not self.request.user.is_ibis_superadmin:
                queryset = queryset.filter(
                    themes__theme__in=[theme.id for theme in self.themes_list]
                )

        group_filter_values = self.request.GET.getlist('group-filter')

        if len(group_filter_values):
            if 'all' in group_filter_values:
                self.selected_filters['groups'] = 'all'
            else:
                self.selected_filters['groups'] = [uuid.UUID(group_id) for group_id in group_filter_values]
                queryset = queryset.filter(themes_groups__group__in=group_filter_values)
        else:
            if not self.request.user.is_ibis_superadmin:
                queryset = queryset.filter(
                    themes_groups__group__in=[group.id for group in self.groups_list]
                )

        issue_filter_values = self.request.GET.getlist('issue-filter')
        if len(issue_filter_values):
            if 'all' in issue_filter_values:
                self.selected_filters['issues'] = 'all'
            else:
                self.selected_filters['issues'] = [uuid.UUID(issue_id) for issue_id in issue_filter_values]
                queryset = queryset.filter(issues__issue__in=issue_filter_values)
        else:
            if not self.request.user.is_ibis_superadmin:
                queryset = queryset.filter(
                    issues__issue__in=[issue.id for issue in self.issues_list]
                )

        return queryset.distinct('id')

    def get_context_data(self, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)

        # Check whether user has edit rights
        context['themes'] = self.themes_list
        context['groups'] = self.groups_list
        context['issues'] = self.issues_list

        context['selected_filters'] = self.selected_filters

        context.update(kwargs)
        return context


class UserSaveRelationsMixin(object):
    """
    Mixin that serves saving all many to many objects related to user
    """

    def save_themes(self):
        # Saving moderator themes
        existing_moderator_themes = [
            str(id) for id in self.object.themes.filter(is_moderator=True).values_list('theme', flat=True)
            ]

        posted_moderator_themes = self.request.POST.getlist('theme_moderator')

        moderator_themes_to_delete = set(existing_moderator_themes).difference(posted_moderator_themes)
        moderator_themes_to_add = set(posted_moderator_themes).difference(existing_moderator_themes)

        if len(moderator_themes_to_delete):
            self.object.themes.filter(theme_id__in=moderator_themes_to_delete, is_moderator=True).delete()

        if len(moderator_themes_to_add):
            ThemeUsers.objects.bulk_create([
                                               ThemeUsers(theme_id=theme_id, user_id=self.object.id, is_moderator=True)
                                               for theme_id in moderator_themes_to_add
                                           ])

        updated_existing_moderator_themes = set(existing_moderator_themes).difference(moderator_themes_to_delete). \
            union(moderator_themes_to_add)

        # Saving member themes
        existing_member_themes = [
            str(id) for id in self.object.themes.filter(is_moderator=False).values_list('theme', flat=True)
            ]
        posted_member_themes = self.request.POST.getlist('theme_member')
        member_themes_to_delete = set(existing_member_themes).difference(posted_member_themes). \
            union(moderator_themes_to_add)

        member_themes_to_add = set(posted_member_themes).difference(existing_member_themes). \
            difference(updated_existing_moderator_themes)

        if len(member_themes_to_delete):
            self.object.themes.filter(theme_id__in=member_themes_to_delete, is_moderator=False).delete()

        if len(member_themes_to_add):
            ThemeUsers.objects.bulk_create([
                                               ThemeUsers(theme_id=theme_id, user_id=self.object.id, is_moderator=False)
                                               for theme_id in member_themes_to_add
                                               ])

        posted_follower_themes = self.request.POST.getlist('theme_follower')
        self.object.themes.update(is_follower=False)
        self.object.themes.filter(theme_id__in=posted_follower_themes).update(is_follower=True)

    def save_groups(self):
        # Saving moderator group
        existing_moderator_groups = [
            str(id) for id in self.object.themes_groups.filter(is_moderator=True).values_list('group', flat=True)
            ]
        posted_moderator_groups = self.request.POST.getlist('group_moderator')
        moderator_groups_to_delete = set(existing_moderator_groups).difference(posted_moderator_groups)
        moderator_groups_to_add = set(posted_moderator_groups).difference(existing_moderator_groups)

        if len(moderator_groups_to_delete):
            self.object.themes_groups.filter(group_id__in=moderator_groups_to_delete, is_moderator=True).delete()
        if len(moderator_groups_to_add):
            GroupUsers.objects.bulk_create([
                                               GroupUsers(group_id=group_id, user_id=self.object.id, is_moderator=True)
                                               for group_id in moderator_groups_to_add
                                               ])

        updated_existing_moderator_groups = set(existing_moderator_groups).difference(moderator_groups_to_delete).\
            union(moderator_groups_to_add)

        # Saving member Group
        existing_member_groups = [
            str(id) for id in self.object.themes_groups.filter(is_moderator=False).values_list('group', flat=True)
            ]
        posted_member_groups = self.request.POST.getlist('group_member')
        member_groups_to_delete = set(existing_member_groups).difference(posted_member_groups).\
            union(moderator_groups_to_add)

        member_groups_to_add = set(posted_member_groups).difference(existing_member_groups).\
            difference(updated_existing_moderator_groups)

        if len(member_groups_to_delete):
            self.object.themes_groups.filter(group_id__in=member_groups_to_delete, is_moderator=False).delete()

        if len(member_groups_to_add):
            GroupUsers.objects.bulk_create([
                GroupUsers(group_id=group_id, user_id=self.object.id, is_moderator=False)
                for group_id in member_groups_to_add
            ])

        posted_follower_groups = self.request.POST.getlist('group_follower')
        self.object.themes_groups.update(is_follower=False)
        self.object.themes_groups.filter(group_id__in=posted_follower_groups).update(is_follower=True)

    def save_issues(self):
        # Saving moderator issues
        existing_moderator_issues = [
            str(id) for id in self.object.issues.filter(is_moderator=True).values_list('issue', flat=True)
        ]
        posted_moderator_issues = self.request.POST.getlist('issue_moderator')

        moderator_issues_to_delete = set(existing_moderator_issues).difference(posted_moderator_issues)
        moderator_issues_to_add = set(posted_moderator_issues).difference(existing_moderator_issues)

        if len(moderator_issues_to_delete):
            self.object.issues.filter(issue_id__in=moderator_issues_to_delete, is_moderator=True).delete()
        if len(moderator_issues_to_add):
            IssueUsers.objects.bulk_create(
                [
                    IssueUsers(issue_id=issue_id, user_id=self.object.id, is_moderator=True, is_follower=True)
                    for issue_id in moderator_issues_to_add
                ]
            )

        updated_existing_moderator_issues = set(existing_moderator_issues).difference(moderator_issues_to_delete). \
            union(moderator_issues_to_add)

        # Saving member Issues
        existing_member_issues = [
            str(id) for id in self.object.issues.filter(is_moderator=False).values_list('issue', flat=True)
            ]

        posted_member_issues = self.request.POST.getlist('issue_member')
        member_issues_to_delete = set(existing_member_issues).difference(posted_member_issues). \
            union(moderator_issues_to_add)

        member_issues_to_add = set(posted_member_issues).difference(existing_member_issues).\
            difference(updated_existing_moderator_issues)

        if len(member_issues_to_delete):
            self.object.issues.filter(issue_id__in=member_issues_to_delete, is_moderator=False).delete()
        if len(member_issues_to_add):
            IssueUsers.objects.bulk_create(
                [
                    IssueUsers(issue_id=issue_id, user_id=self.object.id, is_moderator=False)
                    for issue_id in member_issues_to_add
                    ]
            )

        posted_follower_issues = self.request.POST.getlist('issue_follower')
        self.object.issues.update(is_follower=False)
        self.object.issues.filter(issue_id__in=posted_follower_issues).update(is_follower=True)


class UserCreateView(LoginRequiredMixin, PermissionRequiredMixin, UserSaveRelationsMixin, CreateView):
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = 'users.add_user'
    raise_exception = True
    model = User
    form_class = UserCreateForm

    def get_context_data(self, **kwargs):
        # Set mode to change form behaviour
        kwargs['mode'] = 'create'

        # Check whether user has edit rights
        kwargs['themes'] = Theme.objects.all()
        kwargs['groups'] = Group.objects.all()
        kwargs['issues'] = Issue.objects.all()

        return super(UserCreateView, self).get_context_data(**kwargs)

    @transaction.atomic
    def form_valid(self, form):
        self.object = form.save()

        setup_user_email(self.request, self.object, [])

        send_email_confirmation(self.request, self.object, signup=False)

        self.save_themes()
        self.save_groups()
        self.save_issues()

        return super(ModelFormMixin, self).form_valid(form)

    def get_success_url(self):
        if 'save_edit' in self.request.POST:
            return reverse('users:update_user', kwargs={'pk': self.object.id})
        return reverse('users:list')


class UserEditView(LoginRequiredMixin, PermissionRequiredMixin, UserSaveRelationsMixin, UpdateView):
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = 'users.change_user'
    raise_exception = True
    model = User
    form_class = UserEditForm

    def get_form_kwargs(self):
        kwargs = super(UserEditView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def form_valid(self, form):
        self.object = form.save()

        self.save_themes()
        self.save_groups()
        self.save_issues()

        return super(ModelFormMixin, self).form_valid(form)

    def get_context_data(self, **kwargs):
        # Set mode to change form behaviour
        kwargs['mode'] = 'edit'

        # Check whether user has edit rights
        kwargs['themes'] = Theme.objects.all()
        kwargs['groups'] = Group.objects.all()
        kwargs['issues'] = Issue.objects.all()

        kwargs['moderator_themes'] = self.object.themes.filter(is_moderator=True).values_list('theme', flat=True)
        kwargs['moderator_groups'] = self.object.themes_groups.filter(is_moderator=True).values_list('group', flat=True)
        kwargs['moderator_issues'] = self.object.issues.filter(is_moderator=True).values_list('issue', flat=True)

        kwargs['member_themes'] = self.object.themes.filter(is_moderator=False).values_list('theme', flat=True)
        kwargs['member_groups'] = self.object.themes_groups.filter(is_moderator=False).values_list('group', flat=True)
        kwargs['member_issues'] = self.object.issues.filter(is_moderator=False).values_list('issue', flat=True)

        kwargs['follower_themes'] = self.object.themes.filter(
            is_follower=True
        ).values_list('theme', flat=True)
        kwargs['follower_groups'] = self.object.themes_groups.filter(
            is_follower=True
        ).values_list('group', flat=True)
        kwargs['follower_issues'] = self.object.issues.filter(
            is_follower=True
        ).values_list('issue', flat=True)

        return super(UserEditView, self).get_context_data(**kwargs)

    def get_success_url(self):
        if 'save_edit' in self.request.POST:
            return reverse('users:update_user', kwargs={'pk': self.object.id})
        return reverse('users:list')


