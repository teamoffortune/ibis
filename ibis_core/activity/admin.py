from django.contrib import admin

from ibis_core.activity.models import Event


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    search_fields = ['actor', 'target', 'event_type']
