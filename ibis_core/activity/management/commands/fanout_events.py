import redis
import time

from django.core.management import BaseCommand

from ibis_core.activity.models import Event

redis_connection = redis.StrictRedis(host='0.0.0.0', port='6380', db=1, charset="utf-8", decode_responses=True)


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        for event in Event.objects.all():
            target = event.target
            print(event)
            print(target)

            followers = None
            if target and hasattr(target, 'get_activity_followers'):
                try:
                    followers = target.get_activity_followers()
                    print(followers)
                except NotImplementedError:
                    pass

            if followers:
                for follower_id in followers:
                    print('Try to fanout for follower %s' % follower_id)
                    redis_connection.zadd(
                        'activity:stream:user:%d' % follower_id,
                        float(int(time.mktime(event.created.timetuple()))),
                        event.id
                        )
