from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView

from ibis_core.activity.models import Event


class ActivitiesListView(LoginRequiredMixin, ListView):
    model = Event
    paginate_by = 50
    template_name = 'activities/activities_list.html'

    def get_queryset(self):
        qs = self.model.objects.get_user_activity_stream(self.request.user, limit=self.paginate_by)
        return qs

    def get_context_data(self, **kwargs):
        context = super(ActivitiesListView, self).get_context_data(**kwargs)
        context['user_activities'] = self.get_queryset()
        return context
