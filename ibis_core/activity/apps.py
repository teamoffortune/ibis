from django.apps import AppConfig


class ActivityConfig(AppConfig):
    name = 'ibis_core.activity'

    def ready(self):
        import ibis_core.activity.signals
