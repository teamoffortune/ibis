from __future__ import absolute_import, unicode_literals

import redis
import time
from celery import shared_task
from django.conf import settings

from ibis_core.activity.models import Event

redis_connection = redis.StrictRedis(host='0.0.0.0', port='6380', db=1, charset="utf-8", decode_responses=True)


@shared_task
def fanout_event(event_id):
    try:
        event = Event.objects.get(id=event_id)
    except Event.DoesNotExist:
        print('Event does not exists')
        return

    target = event.target

    followers = None
    if target and hasattr(target, 'get_activity_followers'):
        try:
            followers = target.get_activity_followers()
        except NotImplementedError:
            pass

    if followers:
        for follower_id in followers:
            redis_connection.zadd(
                'activity:stream:user:%d' % follower_id,
                float(int(time.mktime(event.created.timetuple()))),
                event.id
            )
