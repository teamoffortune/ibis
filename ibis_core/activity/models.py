import uuid

import redis
from django.contrib.contenttypes import fields
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.conf import settings
from django.template.loader import render_to_string

from ibis_core.common.models import AbstractBaseModel

EVENT_TYPE_CREATE = 'create'

# redis_connection = redis.StrictRedis.from_url(settings.REDIS_ACTIVITY_URL)
redis_connection = redis.StrictRedis(host='0.0.0.0', port='6380', db=1, charset="utf-8", decode_responses=True)


class EventManager(models.Manager):
    def get_user_activity_stream(self, user, offset=0, limit=11):
        events_ids = redis_connection.zrange('activity:stream:user:%d' % user.id, offset, limit, desc=True)
        if len(events_ids):
            return self.filter(id__in=events_ids).order_by('-created')
        return []


class Event(AbstractBaseModel):
    objects = EventManager()

    actor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='activities'
    )

    target_content_type = models.ForeignKey(ContentType, related_name='event_target')
    target_id = models.UUIDField(default=uuid.uuid4, editable=False)
    target = fields.GenericForeignKey('target_content_type', 'target_id')

    event_type = models.CharField(max_length=200, default=EVENT_TYPE_CREATE)

    def __str__(self):
        template_name = "activities/%s_activity.txt" % self.event_type
        try:
            repr = render_to_string(template_name=template_name, context=dict(
                actor=self.actor,
                target_model=self.target._meta.model_name,
                target_url=self.target.get_absolute_url,
                target_name=self.target.name
            ))
        except AttributeError:
            repr = self.actor

        return repr


class EventMixin(object):
    class Meta:
        abstract = True

    def create_event(self, actor, event_type=EVENT_TYPE_CREATE):
        target_content_type = ContentType.objects.get_for_model(self)
        Event.objects.create(
            actor=actor,
            target_id=self.id,
            target_content_type_id=target_content_type.id,
            event_type=event_type
        )

    def get_followers(self):
        raise NotImplementedError
