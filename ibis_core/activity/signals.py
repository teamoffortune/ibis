from django.db.models.signals import post_save
from django.dispatch import receiver

from ibis_core.activity.models import Event
from ibis_core.activity.tasks import fanout_event


@receiver(post_save, sender=Event)
def fanout_event_signal(sender, instance, created, **kwargs):
    if created:
        fanout_event.delay(instance.id)
