# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url
from django_comments.feeds import LatestCommentFeed

from ibis_core.issues.views import IssueAddExistingSearchView
from ibis_core.searches.views import SearchCreateView
from . import views

urlpatterns = [
    # URL pattern for the IssueListView
    url(
        regex=r'^$',
        view=views.IssueListView.as_view(),
        name='list'
    ),

    # URL pattern for the CreateIssue
    url(
        regex=r'^~new/$',
        view=views.IssueCreateView.as_view(),
        name='create'
    ),

    # URL pattern for creating issue form selected articles
    url(
        regex=r'^~new-issue-articles/$',
        view=views.IssueCreateFromArticlesView.as_view(),
        name='create-from-articles'
    ),

    # URL pattern for add articles ti existing issue
    url(
        regex=r'^~add-articles/$',
        view=views.IssueUpdateFromSearchView.as_view(),
        name='add-articles'
    ),

    # URL pattern for the IssueDetailView
    url(
        regex=r'^(?P<slug>[\w-]+)$',
        view=views.IssueDetailView.as_view(),
        name='detail'
    ),

    url(
        regex=r'^(?P<slug>[\w-]+)/~update$',
        view=views.IssueUpdateView.as_view(),
        name='update'
    ),

    url(
        regex=r'^(?P<slug>[\w-]+)/access/~update$',
        view=views.IssueAccessibilityView.as_view(),
        name='update_access'
    ),


    # Url for issue search
    url(
        regex=r'^(?P<slug>[\w-]+)/~add_search/$',
        view=SearchCreateView.as_view(),
        name='add_issue_search'
    ),

    # Url for adding existing search/searches
    url(
        regex=r'^(?P<slug>[\w-]+)/~add_existing_search/$',
        view=IssueAddExistingSearchView.as_view(),
        name='add_existing_search'
    ),

    url(
        regex=r'^(?P<slug>[\w-]+)/~follow/$',
        view=views.IssueFollowView.as_view(),
        name='issue_follow'
    ),

    url(
        regex=r'^(?P<slug>[\w-]+)/~unfollow/$',
        view=views.IssueUnfollowView.as_view(),
        name='issue_unfollow'
    ),

    # Include latest comments to issues dashboard
    url(r'^feeds/comments/$', LatestCommentFeed(), name='comments-feed'),

]
