from django import template

register = template.Library()


@register.inclusion_tag('issues/templatetags/issue_info.html')
def issue_info(issue):
    issue_groups = []
    issue_themes = []
    for issue in issue.groups.all():
        issue_groups.append(issue.group)
        for theme in issue.group.theme.all():
            issue_themes.append(theme)

    return {'groups': issue_groups, 'themes': issue_themes}
