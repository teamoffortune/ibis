import json
import random
from math import ceil

from django import http
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.db import transaction
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views import View
from django.views.generic import ListView, DetailView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormMixin, FormView, BaseUpdateView
from django.views.generic.list import MultipleObjectMixin
from sphinx.locale import _

from ibis_core.articles.models import Article
from ibis_core.common import constants
from ibis_core.common.constants import PUBLIC
from ibis_core.common.mixins import UserGroupIssueModeratorPermission, UserIssueModeratorPermission, EditRightsPermision, \
    UserGroupModeratorPermission, UserIssueSearchModeratorPermission
from ibis_core.common.utils import get_issues_list_for_user, get_articles_list_for_issue
from ibis_core.groups.models import Group, GroupUsers
from ibis_core.issues.forms import CreateIssueForm, PaginateArticlesForm
from ibis_core.issues.models import Issue, IssueUsers, IssueGroup
from ibis_core.searches.models import Search


PAGINATE_CHOICES = (
    (10, '10'),
    (25, '25'),
    (50, '50'),
    (100, '100')
)


def get_user_allowed_groups(user):
    """
    Retuns groups list where user is moderator or follower
    :param user:
    :return: groups queryset
    """
    return Group.objects.filter(members__user_id=user.id)


class IssueCreateView(LoginRequiredMixin, UserGroupIssueModeratorPermission,
                      CreateView):
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = 'issues.add_issue'
    model = Issue
    fields = ['name', 'access']

    def get_context_data(self, **kwargs):
        """
        Get the context for this view.
        """
        context = super(IssueCreateView, self).get_context_data(**kwargs)
        user = self.request.user
        context['groups'] = get_user_allowed_groups(user)
        return context

    def form_valid(self, form):
        # We should add the user as issue author and moderator
        self.object = form.save()

        issue_author = IssueUsers(
            issue=self.object,
            user=self.request.user,
            is_author=True,
            is_moderator=True,
            is_follower=True
        )
        issue_author.save()

        selected_groups = self.request.POST.getlist('group')
        # Save issue to selected groups if we just create new issue (not from theme form)
        if self.slug_url_kwarg in self.kwargs:
            # Here we create issues for group
            # if slug in url we suppose that issue created from group form
            group_instance = get_object_or_404(
                Group,
                slug=self.kwargs.get(self.slug_url_kwarg)
            )
            selected_groups = set(selected_groups).union(set([str(group_instance.id)]))

        if len(selected_groups):
            IssueGroup.objects.bulk_create([
                                               IssueGroup(group_id=group_id, issue_id=self.object.id)
                                               for group_id in selected_groups
                                            ])
        if self.object.access == PUBLIC:
            self.object.create_event(actor=self.request.user)

        return super(IssueCreateView, self).form_valid(form)

    def get_success_url(self):
        # Sends the user back to the issue list or to the group detail page
        if self.slug_url_kwarg in self.kwargs:
            group_instance = get_object_or_404(
                Group,
                slug=self.kwargs.get(self.slug_url_kwarg)
            )
            return reverse(
                'groups:detail',
                kwargs={'slug': group_instance.slug}
            )
        else:
            return reverse('issues:list')


class IssueCreateFromArticlesView(IssueCreateView):
    fields = ['name', 'access', 'articles']


class IssueUpdateView(LoginRequiredMixin, EditRightsPermision, UpdateView):
    template_name_suffix = '_update_form'
    permission_denied_message = "You doesn't have enough permissions"
    permission_required = 'issues.change_issue'
    model = Issue
    fields = ('name', 'access',)

    @transaction.atomic
    def form_valid(self, form):
        # We should add the user as issue author and moderator
        self.object = form.save()

        selected_groups = self.request.POST.getlist('group')
        existed_groups = [str(id) for id in self.object.groups.all().values_list('group', flat=True)]

        groups_to_add = set(selected_groups) - set(existed_groups)
        groups_to_delete = set(existed_groups) - set(selected_groups)

        if len(groups_to_delete):
            self.object.groups.filter(group_id__in=groups_to_delete).delete()

        if len(groups_to_add):
            IssueGroup.objects.bulk_create([
                                               IssueGroup(group_id=group_id, issue_id=self.object.id)
                                               for group_id in groups_to_add
                                            ])

        return super(IssueUpdateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        """
        Get the context for this view.
        """
        context = super(IssueUpdateView, self).get_context_data(**kwargs)
        user = self.request.user
        context['groups'] = get_user_allowed_groups(user)
        context['selected_groups'] = context['object'].groups.all().values_list('group', flat=True)
        return context

    def get_success_url(self):
        return reverse('issues:list')


class IssueUpdateFromSearchView(FormView):

    def __init__(self):
        super(IssueUpdateFromSearchView, self).__init__()
        self.issue_instance = None

    def get_issue_instance(self):
        if self.issue_instance is not None:
            return self.issue_instance
        pk = self.request.POST.get('pk')
        return get_object_or_404(Issue, pk=pk)

    def get_success_url(self):
        return reverse(
            'issues:detail',
            kwargs={'slug': self.get_issue_instance().slug}
        )

    def post(self, request, *args, **kwargs):
        articles_list = self.request.POST.getlist('articles', None)
        self.issue_instance = self.get_issue_instance()
        if self.issue_instance and articles_list and len(articles_list):
            articles = Article.objects.filter(id__in=articles_list)
            self.issue_instance.articles.add(*articles)
            # search.save()

        return HttpResponseRedirect(self.get_success_url())


class IssueListView(LoginRequiredMixin, ListView):
    model = Issue

    def get_queryset(self):
        # We should return issues where user is member
        q_param = self.request.GET.get('q', '')

        if self.request.user.is_superuser:
            qs = super(IssueListView, self).get_queryset()
            if q_param:
                qs = qs.filter(name__icontains=q_param)
        else:
            qs = get_issues_list_for_user(self.request.user, q_param)

        return qs

    def get_context_data(self, **kwargs):
        context = super(IssueListView, self).get_context_data(**kwargs)

        context['follower_list'] = IssueUsers.objects.filter(
            is_follower=True,
            user=self.request.user
        ).values_list('issue_id', flat=True)

        return context


class IssueDetailView(LoginRequiredMixin, SingleObjectMixin, ListView, FormMixin):
    model = Issue
    paginate_by = 25
    context_object_name = 'articles_list'
    form_class = PaginateArticlesForm
    template_name = "issues/issue_detail.html"
    allow_empty = True

    def get(self, request, *args, **kwargs):
        slug = kwargs.get('slug')
        self.object = self.get_object(queryset=Issue.objects.filter(slug=slug))
        return super(IssueDetailView, self).get(request, *args, **kwargs)

    def get_queryset(self):
        return self.object.articles.all()

    def get_form_kwargs(self):
        return {
            'paginate_choices': PAGINATE_CHOICES
        }

    def get_context_data(self, **kwargs):
        """
        Get the context for this view.
        """
        queryset = kwargs.pop('object_list', self.object_list)

        paginate_by = self.request.GET.get('paginate_by', self.get_paginate_by(queryset))

        try:
            paginate_by = int(paginate_by)
        except ValueError:
            paginate_by = self.paginate_by

        # page_size = self.get_paginate_by(queryset)
        page_size = paginate_by
        context_object_name = self.get_context_object_name(queryset)
        if page_size:
            paginator, page, queryset, is_paginated = self.paginate_queryset(queryset, page_size)
            context = {
                'paginator': paginator,
                'page_obj': page,
                'is_paginated': is_paginated,
                'object_list': queryset,
                'map_items_json': self._get_map_items(page)
            }
        else:
            context = {
                'paginator': None,
                'page_obj': None,
                'is_paginated': False,
                'object_list': queryset,
                'map_items_json': self._get_map_items(self.object_list)
            }
        if context_object_name is not None:
            context[context_object_name] = queryset
        context.update(kwargs)
        return super(IssueDetailView, self).get_context_data(**context)

    # def get_context_data(self, **kwargs):
    #     # self.object_list = get_articles_list_for_issue(self.get_object())[:200]
    #     context = super(IssueDetailView, self).get_context_data(object_list=self.object_list)
    #
    #
    #     obj = self.get_object()
    #     context['issue_groups'] = obj.groups.all()
    #     context['has_edit_rights'] = context['object'].has_edit_rights(self.request.user)
    #
    #     context['page_obj'] = page_obj
    #     context['is_paginated'] = page_obj['has_previous'] or page_obj['has_next']
    #
    #     return context

    def _get_map_items(self, object_list):
        map_items = list()
        zindex_count = 0
        for obj in object_list:
            if obj.locations:
                locations_str = ''.join(obj.locations)
                locations = json.loads(locations_str)
                coord = list()
                for loc in locations['coordinates']:
                    coord.append({
                        'lat': loc['lat'],
                        'lng': loc['lng'],
                        'zindex': zindex_count
                    })
                map_items.append({
                    'name': obj.translated_title,
                    'coord': coord,
                })
        return json.dumps(map_items)


class IssueAccessibilityView(LoginRequiredMixin, EditRightsPermision, UpdateView):
    permission_denied_message = "You doesn't have enough permissions."
    permission_required = 'issues.change_issue'
    model = Issue
    fields = ['access']

    def get_success_url(self):
        issue_instance = self.get_object()
        return reverse(
            'issues:detail',
            kwargs={'slug': issue_instance.slug}
        )


class IssueAddExistingSearchView(LoginRequiredMixin, UserIssueSearchModeratorPermission, FormView):
    permission_required = 'issues.change_issues'
    permission_denied_message = 'You have no permission to add Searches to the Issue.'
    raise_exception = True
    template_name = 'searches/searches_select_list.html'

    def __init__(self):
        super(IssueAddExistingSearchView, self).__init__()
        self.issue_instance = None

    def get_issue_instance(self):
        if self.issue_instance is not None:
            return self.issue_instance
        slug_field = self.kwargs.get('slug')
        return get_object_or_404(Issue, slug=slug_field)

    def get_queryset(self):
        """
        We should compose list of SEARCHES to select except existed issue searches
        """
        self.issue_instance = self.get_issue_instance()
        exclude_list = self.issue_instance.searches.values_list(
            'id',
            flat=True
        )
        return Search.objects.exclude(id__in=exclude_list)

    def get_context_data(self, **kwargs):
        """
        Get the context for this view.
        """
        queryset = self.get_queryset()
        context = {'paginator': None, 'page_obj': None, 'is_paginated': False,
                   'object_list': queryset, 'searches_list': queryset}

        context.update(kwargs)
        if 'view' not in kwargs:
            context['view'] = self
        return context

    def get_success_url(self):
        return reverse(
            'issues:detail',
            kwargs={'slug': self.get_issue_instance().slug}
        )

    def post(self, request, *args, **kwargs):
        search_id_list = self.request.POST.getlist('_selected_searches', None)
        self.issue_instance = self.get_issue_instance()
        if self.issue_instance and search_id_list and len(search_id_list):
            searches = Search.objects.filter(id__in=search_id_list)
            for search in searches:
                search.issues.add(self.issue_instance)
                search.save()

        return HttpResponseRedirect(self.get_success_url())


class IssueFollowView(LoginRequiredMixin, View):

    @transaction.atomic()
    def post(self, request, **kwargs):
        user = request.user
        issue_instance = get_object_or_404(Issue, slug=kwargs['slug'])

        if issue_instance.access == PUBLIC:
            issue_instance.add_follower(user)

        return http.HttpResponseRedirect(reverse('issues:list'))


class IssueUnfollowView(LoginRequiredMixin, View):

    @transaction.atomic()
    def post(self, request, **kwargs):
        user = request.user
        issue_instance = get_object_or_404(Issue, slug=kwargs['slug'])

        if issue_instance.access == PUBLIC:
            try:
                issue_user = IssueUsers.objects.get(user=user, issue=issue_instance)
                issue_user.is_follower = False
                issue_user.save()
            except IssueUsers.DoesNotExist:
                pass

        return http.HttpResponseRedirect(reverse('issues:list'))





