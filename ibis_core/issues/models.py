from django.conf import settings
from django.db import models
from django.urls import reverse

from ibis_core.activity.models import EventMixin
from ibis_core.common.constants import PUBLIC, PRIVATE
from ibis_core.common.model_mixins import ModelAccessMixin
from ibis_core.common.models import AbstractNamedModel


class IssueGroup(models.Model):
    group = models.ForeignKey('groups.Group', related_name='issues')
    issue = models.ForeignKey(
        'Issue',
        related_name='groups'
    )


class IssueUsers(models.Model):
    issue = models.ForeignKey('Issue', related_name='members')
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='issues'
    )
    is_author = models.BooleanField(default=False)
    is_moderator = models.BooleanField(default=False)
    is_follower = models.BooleanField(default=False)


class Issue(AbstractNamedModel, ModelAccessMixin, EventMixin):
    ACCESS_CHOICES = (
        (PUBLIC, 'Public'),
        (PRIVATE, 'Private')
    )

    DEFAULT_ACCESS_CHOICE = PUBLIC

    articles = models.ManyToManyField('articles.Article', related_name='issue_articles', blank=True, db_index=True)

    allow_comments = models.BooleanField('Allow comments', default=True)

    def moderator_list(self):
        return [issue_member.user for issue_member in self.members.filter(is_moderator=True).all()]

    def member_list(self):
        return [issue_member.user for issue_member in self.members.all()]

    def has_edit_rights(self, user):
        return user in self.moderator_list()

    @models.permalink
    def get_absolute_url(self):
        return 'issues:detail', (), {'slug': self.slug}

    def get_activity_followers(self):
        followers = [user_id for user_id in self.members.values_list('user_id', flat=True)]
        if self.access == PUBLIC:
            [followers.extend(issue_group.group.get_activity_followers()) for issue_group in self.groups.all()]

        return list(set(followers))

    def add_follower(self, user):
        if self.access == PUBLIC:
            try:
                issue_user = IssueUsers.objects.get(user=user, issue=self)
                issue_user.is_follower = True
                issue_user.save()
            except IssueUsers.DoesNotExist:
                IssueUsers.objects.create(user=user, issue=self, is_follower=True)

            for issue_group in self.groups.all():
                issue_group.group.add_follower(user)


