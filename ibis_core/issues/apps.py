from django.apps import AppConfig


class IssuesConfig(AppConfig):
    name = 'ibis_core.issues'
