from django.contrib import admin

from ibis_core.issues.models import Issue


@admin.register(Issue)
class IssueAdmin(admin.ModelAdmin):
    search_fields = ['name']
    exclude = ('articles',)
