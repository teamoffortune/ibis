from django import forms

from ibis_core.issues.models import Issue


class CreateIssueForm(forms.ModelForm):
    """
    Form for creating an Issue and bind chosen articles to it.
    """
    class Meta:
        model = Issue
        fields = ('name', 'access',)


class PaginateArticlesForm(forms.Form):
    def __init__(self, paginate_choices, *args, **kwargs):
        super(PaginateArticlesForm, self).__init__(*args, **kwargs)
        self.fields['paginate_by'].choices = paginate_choices

    paginate_by = forms.ChoiceField(
        required=False,
        widget=forms.Select(attrs={'class': 'form-control', 'form': ''}),
        initial=25
    )
