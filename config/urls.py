# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views

from ibis_core.activity.views import ActivitiesListView
from ibis_core.common.views import SuggestView, GlobalSearchView, ThemeChidlrensView, ThemeListViewAPI, \
    UserManagementAPI
from ibis_core.dashboard.views import DashboardView
from ibis_core.searches.views import TestGoogleCSEQuery
from ibis_core.themes.views import ThemeListView

urlpatterns = [
    url(r'^$', DashboardView.as_view(), name='dashboard'),
    url(r'^about/$', TemplateView.as_view(template_name='pages/about.html'),
        name='about'),

    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, include(admin.site.urls)),

    url(r'^search/', view=GlobalSearchView.as_view(), name='global_search'),

    # User management
    url(r'^users/', include('ibis_core.users.urls', namespace='users')),

    # Themes management
    url(r'^themes/', include('ibis_core.themes.urls', namespace='themes')),

    # Groups management
    url(r'^groups/', include('ibis_core.groups.urls', namespace='groups')),

    # Issues management
    url(r'^issues/', include('ibis_core.issues.urls', namespace='issues')),

    # Searches management
    url(r'^searches/', include('ibis_core.searches.urls', namespace='searches')),

    # Articles management
    url(r'^articles/', include('ibis_core.articles.urls', namespace='articles')),

    url(r'^feedback/', include('ibis_core.feedback.urls', namespace='feedbacks')),

    # Daily Digest management
    url(r'^digest/', include('ibis_core.digest.urls', namespace='digest')),

    # User accounts management
    url(r'^accounts/', include('allauth.urls')),

    url(r'^activities/', view=ActivitiesListView.as_view(), name='activities_list'),



    url(r'^api/v1/testquery',
        view=TestGoogleCSEQuery.as_view(),
        name='api_testquery'),

    url(r'^api/v1/suggest',
        view=SuggestView.as_view(),
        name='api_suggest'),


    url(r'^api/v1/theme/(?P<slug>[\w-]+)$',
        view=ThemeChidlrensView.as_view(),
        name='api_um_theme'),

    url(r'^api/v1/themes$',
        view=ThemeListViewAPI.as_view(),
        name='api_um_themes'),

    url(r'^api/v1/umdata$',
        view=UserManagementAPI.as_view(),
        name='api_um_data'),





    # Your stuff: custom urls includes go here

    # Summernote WYSIWYG editor URL's
    url(r'^summernote/', include('django_summernote.urls')),

    # Included Django Comments
    url(r'^comments/', include('django_comments_xtd.urls')),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied,
            kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found,
            kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
