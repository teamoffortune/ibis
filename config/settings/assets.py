PIPELINE = {
    'STYLESHEETS': {
        'main_css': {
            'source_filenames': (
               'css/project.css',
               'css/bootstrap.css',
               'css/search.css',
               'js/libs/feedback/feedback.css',
               'css/tagging.css',
            ),
            'output_filename': 'css/main.css',
        },
        'dashboard_css': {
            'source_filenames': (
               'js/libs/DataTables/datatables.min.css',
            ),
            'output_filename': 'css/dashboard.css',
        },
        'no_script_css': {
            'source_filenames': (
               'css/no_script.css',
            ),
            'output_filename': 'css/no_script.css',
        },
        'searches_css': {
            'source_filenames': (
               'css/search_page.css',
               'css/bootstrap-multiselect.css',
            ),
            'output_filename': 'css/searches.css',
        },
        'user_management_css': {
            'source_filenames': (
                'js/libs/multiselect/css/multi-select.css',
                'js/libs/bootstrap-table/src/bootstrap-table.css',
            ),
            'output_filename': 'css/um.css',
        },
        'user_edit_css': {
            'source_filenames': (
                'js/libs/multiselect/css/multi-select.css',
            ),
            'output_filename': 'css/user_edit.css',
        },
        'article_edit_css': {
            'source_filenames': (
                'js/libs/tags-input/bootstrap-tagsinput.css',
            ),
            'output_filename': 'css/article_edit.css',
        }
    },
    'JAVASCRIPT': {
        'main_js': {
            'source_filenames': (
                'js/bootstrap.js',
                'js/libs/feedback/feedback.js',
                'js/libs/underscore/underscore-min.js'
            ),
            'output_filename': 'js/main.js',
        },
        'dashboard_js': {
            'source_filenames': (
                'js/libs/DataTables/datatables.min.js',
                'js/pages/dashboard.js'
            ),
            'output_filename': 'js/dashboard.js',
        },
        'user_management_js': {
            'source_filenames': (
                'js/libs/bootstrap-table/src/bootstrap-table.js',
                'js/libs/multiselect/js/jquery.multi-select.js',
                'js/libs/bootstrap-table/dist/extensions/filter-control/bootstrap-table-filter-control.js',
                'js/pages/user_management.js',
            ),
            'output_filename': 'js/page_um.js',
        },
        'user_edit_js': {
            'source_filenames': (
                'js/libs/multiselect/js/jquery.multi-select.js',
                'js/pages/user_edit.js',
            ),
            'output_filename': 'js/page_user_edit.js',
        },
        'search_edit_js': {
            'source_filenames': (
                'js/libs/twig.js/twig.min.js',
                'js/pages/search_edit.js',
                'js/libs/bootstrap-multiselect.js',
            ),
            'output_filename': 'js/page_se.js',
        },
        'issue_detail_js': {
            'source_filenames': (
                'js/map/map.js',
            ),
            'output_filename': 'js/issue_detail.js',
        },
        'article_detail_js': {
            'source_filenames': (
                'js/map/map.js',
                'js/pages/article_detail.js',
            ),
            'output_filename': 'js/article_detail.js',
        },
        'article_edit_js': {
            'source_filenames': (
                'js/libs/twig.js/twig.min.js',
                'js/libs/tags-input/bootstrap-tagsinput.js',
                'js/pages/article_edit.js',
            ),
            'output_filename': 'js/article_edit.js',
        },
        'filtered_search_js': {
            'source_filenames': (
                'js/pages/filtered_search.js',
            ),
            'output_filename': 'js/article_edit.js',
        },
    }
}
